#include <iostream>
#include <memory>
#include <string>
#include<cstring>
#include<stack>
#include<vector>
#include<map>
#include <algorithm>

// ���� AST �Ļ���
using namespace std;
static map<string,string> symbol;
static string tmps="";
static stack<string> st;
static map<string,int> retn;
static stack<string> st1;
static stack<int> imm;
static int flag=1;
static stack<int> ret;
static string fun;
static vector<string> val;
static vector<string> funcval;
static map<string,vector<int>> arr;
static int num=0;
static int num1=-1;
static int last_num=0;
static int ret_num=0;
static int flag1=0;
static int intable=0;
static int sum=0;
static int rr=0;
static string varname="";
static stack<int> q;
static stack<string> symbol_tmp;
static string tmpres="";
static stack<int> inwhile;
static int blockdepth=1;
static int maxd=0;
static vector<map<string,string>> symbold;
static vector<map<string,string>> const_symbold;
static vector<std::pair<string,vector<int>>> funcname;
static map<string,int> line;
static int whilepos=0;
static int breakpos=0;
static int isvoid=0;
static int funcrnum=0;
static int infunc=0;
static int isglobal=1;
static int globalconst=0;
static int ik=0;
static vector<int> inb;
static int param=0;
static int isfun=0;
static int duanlu=0;
static int isparam=0;
static vector<string> arrs;
static vector<string> arrparam;
static map<string,vector<int>> arrsize;
static int inmain=0;
static int isjump=0;
static int boolnum=0;
class BaseAST {
 public:
  static string out;
  virtual ~BaseAST() = default;
  virtual void Dump()=0;
};
// CompUnit �� BaseAST

class CompUnitsAST : public BaseAST {
 public:
  // ������ָ���������
  unique_ptr<BaseAST> comp1;  
  unique_ptr<BaseAST> comp2;
  int mode;
  void Dump(){
    symbold.push_back(*(new map<string, string>()));
    const_symbold.push_back(*(new map<string, string>()));
    vector<int> mm;mm.push_back(0);mm.push_back(0);string libs="getint";
    funcname.push_back(std::pair(libs,mm));mm.clear();
    mm.push_back(0);mm.push_back(0);libs="getch";
    funcname.push_back(std::pair(libs,mm));mm.clear();
    mm.push_back(2);mm.push_back(1);libs="getarray";
    funcname.push_back(std::pair(libs,mm));mm.clear();
    mm.push_back(1);mm.push_back(1);libs="putint";
    funcname.push_back(std::pair(libs,mm));mm.clear();
    mm.push_back(1);mm.push_back(1);libs="putch";
    funcname.push_back(std::pair(libs,mm));mm.clear();
    mm.push_back(2);mm.push_back(2);libs="putarray";
    funcname.push_back(std::pair(libs,mm));mm.clear();
    mm.push_back(1);mm.push_back(0);libs="starttime";
    funcname.push_back(std::pair(libs,mm));mm.clear();
    mm.push_back(1);mm.push_back(0);libs="stoptime";
    funcname.push_back(std::pair(libs,mm));mm.clear();
    out+="decl @getint(): i32\ndecl @getch(): i32\ndecl @getarray(*i32): i32\ndecl @putint(i32)\ndecl @putch(i32)\ndecl @putarray(i32, *i32)\ndecl @starttime()\ndecl @stoptime()\n";
    if(mode==0){
    comp1->Dump();
    }
    if(mode==1){
      comp1->Dump();
      comp2->Dump();
    }
    

  }
};
class CompUnitAST : public BaseAST {
 public:
  // ������ָ���������
  unique_ptr<BaseAST> func_def;
  unique_ptr<BaseAST> comp;
  int mode;
  void Dump(){

    if(mode==0){
    func_def->Dump();
    }
    if(mode==1){
      comp->Dump();
      func_def->Dump();
    }
    if(mode==2){
    
    func_def->Dump();
    }
    if(mode==3){
      comp->Dump();
      func_def->Dump();
    }

  }
};

// FuncDef Ҳ�� BaseAST
class FuncDefAST : public BaseAST {
 public:
  //unique_ptr<BaseAST> func_type;
  string s;
  string ident;
  unique_ptr<BaseAST> block;
  unique_ptr<BaseAST> func;
  int mode;
  void Dump(){
    auto arr_=arr;
    auto arrsize_=arrsize;
    fun=ident;
    retn[fun]=-1;
    isglobal=0;flag=0;
    string s1="fun ";arrs.clear();
    out+=s1;
    //int arrsize1=arr.size();
    
    symbold.push_back(*(new map<string, string>()));
    const_symbold.push_back(*(new map<string, string>()));
    out+=string("@")+ident+string("(");
    cout<<ident<<endl;
    if(ident=="main")inmain=1;
    if(mode==1)flag=1,func->Dump();
    out+=")";flag=0;
    
    //func_type->Dump();
    out+=s+string(" ");
    if(s=="")isvoid=1;
    out+=string("{\n");
    num+=1;
    vector<int> mm;mm.push_back(isvoid),mm.push_back(funcrnum);funcrnum=0;
    funcname.push_back(std::pair(ident,mm));
    out+=string("%entry")+to_string(num)+string(":\n");
    //cout<<imm.size()<<"asdp\n";
    int tmp5=0;
    vector<string> arr1;
    for(auto i=funcval.begin(); i != funcval.end(); i++){
      
      if((*i).at(0)=='?'){
        string tmp2=(*i).substr(1,(*i).length());
        out+="@"+tmp2+"_"+to_string(blockdepth)+" = alloc *i32\n";
        out+="store @"+tmp2+",@"+tmp2+"_"+to_string(blockdepth)+"\n";
        symbold[blockdepth][tmp2+"_"]="-114514";
        arrparam.push_back(tmp2);vector<int> tt;tt.push_back(-1);arr[tmp2+"_"+to_string(blockdepth)]=tt;
      }
      else if((*i).at(0)=='!'){
        string tmp2=(*i).substr(1,(*i).length());
        out+="@"+tmp2+"_"+to_string(blockdepth)+" = alloc *";
        out+=arrs[tmp5]+"\n";tmp5++;
        out+="store @"+tmp2+",@"+tmp2+"_"+to_string(blockdepth)+"\n";
        arrparam.push_back(tmp2);vector<int> tt;tt.push_back(-1);arr[tmp2+"_"+to_string(blockdepth)]=tt;
        symbold[blockdepth][tmp2+"_"]="-114514";
        arr1.push_back(tmp2+"_"+to_string(blockdepth));
      }
      else{
      out+="@"+*i+"_"+to_string(blockdepth)+" = alloc i32\n";
      out+="store @"+*i+",@"+*i+"_"+to_string(blockdepth)+"\n";
      symbold[blockdepth][*i+"_"]="-114514";
      }
    }
    //param=1;
    
    block->Dump();
    
    if(isvoid&&rr==0){
      out+="ret \n";isvoid=0;
    }
    else if(rr==0){
      out+="ret 0\n";
    }
    out+=string("}\n");
    funcval.clear();
    symbold.pop_back();
    const_symbold.pop_back();
    rr=0;
    isglobal=1;flag=1;//param=0;
    arrparam.clear();val.clear();
    /*auto ii=arr.begin();
    while(arrsize1){
      ii++;arrsize1--;
    }
    for(;ii!=arr.end(); ++ii){
      vector<int> kk;
      arr[(*ii).first]=kk;
    }
    for(auto ii1=arr1.begin();ii1!=arr1.end();ii1++){
        vector<int> kk;
        arrsize[(*ii1)]=kk;
    } */
    arr=arr_;
    arrsize=arrsize_;
  }
};

class FuncTypeAST : public BaseAST {
 public:
  // ������ָ���������
  string s;
  void Dump(){


    out+=s+string(" ");
    if(s=="")isvoid=1;
  }
};
class FuncFParamsAST : public BaseAST {
 public:
  unique_ptr<BaseAST> constdef;
  unique_ptr<BaseAST> constdefs;
  int mode;
  void Dump(){
    //param=1;
    if(mode==0)constdef->Dump(),funcrnum++;
    else{
      constdefs->Dump();out+=",";
      constdef->Dump();funcrnum++;
    }
    //param=0;
  }
};
class FuncRParamsAST : public BaseAST {
 public:
  unique_ptr<BaseAST> constdef;
  unique_ptr<BaseAST> constdefs;
  int mode;
  void Dump(){
    if(mode==0)constdef->Dump();
    else{
      constdefs->Dump();
      constdef->Dump();
    }
    
  }
};
class FuncFParamAST : public BaseAST {
 public:
  
  string s;
  int mode;
  unique_ptr<BaseAST> exp;
  void Dump(){
    //out+=s;

    string outt="";
    if(mode==0){
    outt+="@"+s+":i32";
    funcval.push_back(s);
    }
    if(mode==1){
      outt+="@"+s+":*";
      funcval.push_back("!"+s);
      int size1=st.size();
      exp->Dump();
       vector<int> dy;inb.clear();
      while(st.size()>size1){
        dy.push_back(imm.top());imm.pop();st.pop();
      }
      string lefti[1000],righti[1000];
      for(int i=0;i<dy.size();i++){
          if(i==dy.size()-1)lefti[i]="[i32",righti[i]=","+to_string(dy[i])+"]";
          else lefti[i]="[",righti[i]=","+to_string(dy[i])+"]";
      }
      string outtt="";
      for(int i=0;i<dy.size();i++){
        outt+=lefti[i];outtt+=lefti[i];
      }
      for(int i=0;i<dy.size();i++){
        outt+=righti[i];outtt+=righti[i];
      }
      arrs.push_back(outtt);
      for(int i=0;i<dy.size()-1;i++){
        int nn=1;
        for(int j=0;j<=i;j++){
          nn*=dy[j];
        }
        inb.push_back(nn);
      }
      inb.push_back(0);
      arrsize[s+"_"+to_string(blockdepth)]=inb;
      //cout<<s+"_"+to_string(blockdepth)<<" "<<inb.size()<<endl;

    }
    if(mode==2){
      
      outt+="@"+s+":*i32";
      funcval.push_back("?"+s);
    }
    out+=outt;

  }
};


class BlockAST : public BaseAST {
 public:
  // ������ָ���������
  //string s1;
  unique_ptr<BaseAST> blockitems;
  int mode;
  //string entry;
  void Dump(){

    if(mode==0){
    
    
    //out+="\n";
    //blockdepth+=1;if(blockdepth>maxd)maxd=blockdepth;
    blockitems->Dump();
    //blockdepth-=1;

    //out+="}";
    if(rr){return;}
    }
  }
};

class StmtAST : public BaseAST {
 public:
  unique_ptr<BaseAST> exp;
  string s;
  int mode;
  unique_ptr<BaseAST> block;
  unique_ptr<BaseAST> stmt1;
  unique_ptr<BaseAST> stmt2;
  void Dump(){
    if(isjump==1)return;
    if(rr==1&&flag1==0){return;}
    string outt="";
    if(mode==0){
      rr=1;  
      exp->Dump();

      if(st.size()!=0){

        outt+="ret "+st.top()+"\n";
        //if(st.top()[0]!='%')imm.push(atoi(st.top().c_str()));
        if(st.top()[0]!='%')retn[fun]=atoi(st.top().c_str());
        st.pop();imm.pop();

      }
      //else if(tmpres!="")out+=tmpres+"\n";
      else outt+="ret %"+to_string(ret_num)+"\n";
      intable=0;
    }
    if(mode==1){
      int i=0;
      for(i=blockdepth;i>=0;i--){
        if(symbold[i].find(s+"_")!=symbold[i].end())break;
      }
      exp->Dump();
    
    if(imm.size()!=0){symbold[i][s+"_"]=st.top();outt+="store "+st.top()+",@"+s+"_"+to_string(i)+"\n";
    if(st.top()[0]=='%')line["%"+to_string(ret_num)]=imm.top();
    st.pop();imm.pop();}
    else symbold[i][s+"_"]="%"+to_string(ret_num),outt+="store %"+to_string(ret_num)+",@"+s+"_"+to_string(i)+"\n",line["%"+to_string(ret_num)]=imm.top(),imm.pop();
    
    }
    if(mode==2){
      exp->Dump();
      
    }
    if(mode==3){blockdepth++;
      symbold.push_back(*(new map<string, string>()));
      const_symbold.push_back(*(new map<string, string>()));
      block->Dump();
      symbold.pop_back();
      const_symbold.pop_back();
      blockdepth--;

    }
    if(mode==5){inmain=0;
      exp->Dump();inmain=1;
      num+=1;flag1=1;string out1="",out2="";int tmp1=0;
      if (st.size()!=0)out+="br "+st.top()+",%entry"+to_string(num),num++,st.pop(),imm.pop();
      else out+="br %"+to_string(ret_num)+",%entry"+to_string(num),num++;tmp1=num;
      out+=",%entry"+to_string(num)+"\n";
      out+="%entry"+to_string(num-1)+":\n";

      stmt1->Dump();//if(rr){flag1=0;return;}
      if(!rr&&isjump==0)
      out+="jump %entry"+to_string(tmp1)+"\n";
      out+="%entry"+to_string(tmp1)+":\n";
      rr=0,flag1=0;isjump=0;
    }
    if(mode==6){inmain=0;
      exp->Dump();inmain=1;
      num+=1;flag1=1;string out1="";int tmp2=0;
      if (st.size()!=0)out+="br "+st.top()+",%entry"+to_string(num),num++,st.pop(),imm.pop();
      else out+="br %"+to_string(ret_num)+",%entry"+to_string(num),num++;
      out+=",%entry"+to_string(num)+"\n";
      tmp2=num;
      out+="%entry"+to_string(num-1)+":\n";num+=1;
      int reti=0;
      stmt1->Dump();
      out+=out1;if(rr)reti++;
      if(!rr&&isjump==0)
      out+="jump %entry"+to_string(tmp2+1)+"\n";
      rr=0;
      out+="%entry"+to_string(tmp2)+":\n";
      stmt2->Dump();if(rr)reti++;
      if(!rr&&isjump==0)
      out+="jump %entry"+to_string(tmp2+1)+"\n";
      cout<<reti<<endl;
      if(reti<2)
      out+="%entry"+to_string(tmp2+1)+":\n",rr=0;
      flag1=0;isjump=0;
    }
    if(mode==11){
      out+="ret\n";
      rr=1;
    }
    if(mode==7){
      num+=1;out+="jump %while"+to_string(num)+"\n";out+="%while"+to_string(num)+":\n";
      inmain=0;
      exp->Dump();inmain=1;
      num+=1;flag1=1;string out1="",out2="";int tmp1=0;
      if (st.size()!=0)out+="br "+st.top()+",%entry"+to_string(num),num++,st.pop(),imm.pop();
      else out+="br %"+to_string(ret_num)+",%entry"+to_string(num),num++;
      tmp1=num;
      out+=",%entry"+to_string(num)+"\n";
      out+="%entry"+to_string(num-1)+":\n";
      inwhile.push(tmp1);
      stmt1->Dump();
      
      if(!rr&&isjump==0){
        if(isjump==1)out+="%entry_b"+to_string(num)+":\n";
      out+="jump %while"+to_string(tmp1-2)+"\n";
      }
      out+="%entry"+to_string(tmp1)+":\n";
      rr=0,flag1=0;isjump=0;inwhile.pop();
    }
    if(mode==8){
      out+="jump %entry"+to_string(inwhile.top())+"\n";isjump=1;
      //out+="%entry_bb"+to_string(num)+":\n";
    }
    if(mode==9){
      out+="jump %while"+to_string(inwhile.top()-2)+"\n";
      out+="%entry_c"+to_string(num)+":\n";
    }
    if(mode==10){
      int size1=st.size();
      exp->Dump();
      
      int i=0;
      for(i=blockdepth;i>=0;i--){
        if(symbold[i].find(s+"_")!=symbold[i].end())break;
      }
     vector<string> tmp2;
      vector<int> tmp1;int tmp3=num1;
      while(st.size()>size1)tmp1.push_back(imm.top()),tmp2.push_back(st.top()),imm.pop(),st.pop();
      for(int k=tmp1.size()-1;k>=0;k--){
          num1+=1;//cout<<s<<tmp2[k]<<endl;
          if(k==tmp1.size()-1){
            
            if(find(arrparam.begin(),arrparam.end(),s)!=arrparam.end()){//cout<<s<<"arrparam\n";
              out+="%"+to_string(num1)+" = load @"+s+"_"+to_string(i)+"\n";
              num1+=1;
              out+="%"+to_string(num1)+"= getptr %"+to_string(num1-1)+","+tmp2[k]+"\n";
            }
            else
            out+="%"+to_string(num1)+"= getelemptr @"+s+"_"+to_string(i)+","+tmp2[k]+"\n";
          }
          else {
            out+="%"+to_string(num1)+"= getelemptr %"+to_string(num1-1)+","+tmp2[k]+"\n";
          }
        }tmp3=num1;
      stmt1->Dump();
      cout<<st.size()<<"!!\n";
    if(st.size()!=0){symbold[i][s+"_"]=st.top();outt+="store "+st.top()+",%"+to_string(tmp3)+"\n";
    if(st.top()[0]=='%')line["%"+to_string(ret_num)]=imm.top();
    st.pop();imm.pop();
    }
    //else symbold[i][s+"_"]="%"+to_string(ret_num),outt+="store %"+to_string(ret_num)+",@"+s+"_"+to_string(i)+"\n",line["%"+to_string(ret_num)]=imm.top(),imm.pop(),cout<<"line"<<line["%"+to_string(ret_num)]<<endl;

      
    }
    out+=outt;
  }
};
class ExpAST : public BaseAST {
 public:
  unique_ptr<BaseAST> unaryexp;
  int mode;
  void Dump(){
    
    unaryexp->Dump();

  }
};

class PrimaryExpAST : public BaseAST {
 public:
  unique_ptr<BaseAST> exp;
  int mode;
  int n;
  string s;
  void Dump(){
    
    if(mode==1){
      st.push(to_string(n));
      imm.push(n);
      sum=n;
      //if(infunc)st1.push(to_string(n));
    }
    else if(mode==0){
      exp->Dump();
    }
    else if(mode==2){
      int isconst=0;int i=blockdepth,j=blockdepth;
      for(;i>=0;i--){
        if(const_symbold[i].find(s+"_")!=const_symbold[i].end()){isconst=1;break;}
      }
      for(;j>=0;j--){
        if(symbold[j].find(s+"_")!=symbold[j].end())break;
      }

      if(isconst){
        if(arr.find(s+"_"+to_string(i))!=arr.end()&&arr[s+"_"+to_string(i)].size()>0){
          num1+=1;
          out+="%"+to_string(num1)+"= getelemptr @"+s+"_"+to_string(i)+","+to_string(0)+"\n";
          /*int tmp4=num1;
          for(int k=0;k<arrsize[s+"_"+to_string(i)].size();k++){
          num1+=1;
          out+="%"+to_string(num1)+"= getelemptr %"+to_string(tmp4)+","+to_string(0)+"\n";
          tmp4=num1;
          }*/
          st.push("%"+to_string(num1));imm.push(arr[s+"_"+to_string(i)][0]);
        }
        else{
        st.push(const_symbold[i][s+"_"]);
        int ss=atoi(const_symbold[i][s+"_"].c_str());
        imm.push(ss);
        }
        //if(infunc)st1.push(const_symbold[i][s+"_"]);
      }
      else{//cout<<out<<endl;
      cout<<s+"_"+to_string(j)<<" "<<arr[s+"_"+to_string(j)].size()<<endl;
        if(arr.find(s+"_"+to_string(j))!=arr.end()&&arr[s+"_"+to_string(j)].size()>0){
          if(find(arrparam.begin(),arrparam.end(),s)==arrparam.end()){
          num1+=1;
          out+="%"+to_string(num1)+"= getelemptr @"+s+"_"+to_string(j)+","+to_string(0)+"\n";
          /*int tmp4=num1;
          for(int k=0;k<arrsize[s+"_"+to_string(j)].size();k++){
          num1+=1;
          out+="%"+to_string(num1)+"= getelemptr %"+to_string(tmp4)+","+to_string(0)+"\n";
          tmp4=num1;
          }*/
          st.push("%"+to_string(num1));imm.push(arr[s+"_"+to_string(j)][0]);
          }
          else{
            num1+=1;
            out+="%"+to_string(num1)+"= load @"+s+"_"+to_string(j)+"\n";
            num1+=1;
            out+="%"+to_string(num1)+"= getptr %"+to_string(num1-1)+","+to_string(0)+"\n";
            
            st.push("%"+to_string(num1));imm.push(arr[s+"_"+to_string(j)][0]);
          }
        }
        else{
        num1+=1;st.push("%"+to_string(num1));//if(infunc)st.push("%"+to_string(num1));
        if(flag==0)
        out+="%"+to_string(num1)+" = load @"+s+"_"+to_string(j)+"\n";
        if(symbold[j][s+"_"][0]!='%')
        imm.push(atoi(symbold[j][s+"_"].c_str()));
        else imm.push(line[symbold[j][s+"_"]]);
        //cout<<symbold[j][s+"_"].length()<<endl;
        //if(symbold[j][s+"_"]!=""&&symbold[j][s+"_"][0]!='%'){int ss=atoi(symbold[j][s+"_"].c_str());imm.push(ss);}
        //cout<<atoi(symbold[j][s+"_"].c_str())<<endl;
        //out+="call @putint(%"+to_string(num1)+")\n";
        ret_num=num1;
        }//cout<<out<<endl;
      }
    }
    else if(mode==3){//cout<<"test2\n";
      int size1=st.size();
      exp->Dump();
      
      int isconst=0;
      int i=blockdepth;int j=blockdepth;
      for(;i>=0;i--){
        if(const_symbold[i].find(s+"_")!=const_symbold[i].end()){isconst=1;break;}
      }
      for(;j>=0;j--){
        if(symbold[j].find(s+"_")!=symbold[j].end())break;
      }
      vector<string> tmp2;
      vector<int> tmp1;
      while(st.size()>size1)tmp1.push_back(imm.top()),tmp2.push_back(st.top()),imm.pop(),st.pop();
      //int tmp1=imm.top();imm.pop();
      //cout<<out<<endl;
      //cout<<isconst<<"isconst"<<j<<endl;
      //string tmp2=st.top();st.pop();
      if(isconst==0){
        //cout<<tmp1.size()<<"lpasfh\n";
        for(int k=tmp1.size()-1;k>=0;k--){
          num1+=1;//cout<<s<<tmp2[k]<<isparam<<endl;
          if(k==tmp1.size()-1){
            
            if(find(arrparam.begin(),arrparam.end(),s)!=arrparam.end()){//cout<<s<<"arrparam\n";
              out+="%"+to_string(num1)+" = load @"+s+"_"+to_string(j)+"\n";
              num1+=1;
              out+="%"+to_string(num1)+"= getptr %"+to_string(num1-1)+","+tmp2[k]+"\n";
            }
            else
            out+="%"+to_string(num1)+"= getelemptr @"+s+"_"+to_string(j)+","+tmp2[k]+"\n";
          }
          else {
            out+="%"+to_string(num1)+"= getelemptr %"+to_string(num1-1)+","+tmp2[k]+"\n";
          }
        }
      
      
      //cout<<j<<"load\n";
        if(tmp1.size()<arrsize[s+"_"+to_string(j)].size()+1){
        num1+=1;
        out+="%"+to_string(num1)+"= getelemptr %"+to_string(num1-1)+","+to_string(0)+"\n";
        }
        else{
          num1+=1;
          out+="%"+to_string(num1)+" = load %"+to_string(num1-1)+"\n";
        }
      st.push("%"+to_string(num1));
      int tmp3=0;
      int l=0;
      //cout<<"asdag"<<arrsize[s+"_"+to_string(j)].size()<<" "<<tmp1.size()<<endl;
      for(int k=arrsize[s+"_"+to_string(j)].size()-1;k>=0;k--){
        tmp3+=arrsize[s+"_"+to_string(j)][k]*tmp1[tmp1.size()-l-1];l+=1;
        //cout<<"tmp3"<<tmp3<<endl;
      }
      tmp3+=tmp1[0];int de;
      //cout<<out<<endl;
      //cout<<endl<<s+"_"+to_string(j)<<" "<<tmp3<<endl;
      if(find(arrparam.begin(),arrparam.end(),s)!=arrparam.end()||tmp3<0)de=0;
      else de=arr[s+"_"+to_string(j)][tmp3];
      //cout<<tmp3<<" "<<de<<endl;
      imm.push(de);//cout<<"end\n";
      ret_num=num1;//cout<<imm.size()<<endl;
      }
      else{
        num1+=1;
        out+="%"+to_string(num1)+"= getelemptr @"+s+"_"+to_string(i)+",";

      out+=tmp2[0]+"\n";
      num1+=1;
      out+="%"+to_string(num1)+" = load %"+to_string(num1-1)+"\n";
      st.push("%"+to_string(num1));
      imm.push(arr[s+"_"+to_string(i)][tmp1[0]]);
      ret_num=num1;
      }
      
      //cout<<out<<endl;
    }
  }
};

class UnaryExpAST : public BaseAST {
 public:
  unique_ptr<BaseAST> exp1;
  unique_ptr<BaseAST> exp2;
  unique_ptr<BaseAST> funcr;
  string s;
  int mode;
  void Dump(){
    if(mode==0){
      exp1->Dump();
    }
    if(mode==1){
      
      exp2->Dump();
      exp1->Dump();
    }
    if(mode==2){
      //cout<<isfun<<"isfun\n";
      if(isfun==1){isfun=0,duanlu=1;return;}
      int haveret=0;auto ii=funcname.begin();
      for(auto i=funcname.begin(); i != funcname.end(); i++){
        if((*i).first==s)haveret=(*i).second[0],ii=i;
      }isparam=1;
      funcr->Dump();
      isparam=0;
      
      
      
      stack<string> rev;int times=(*ii).second[1];
      
      while(times){
        
        rev.push(st.top());//cout<<times<<" "<<st.top()<<" "<<st.size()<<" "<<imm.size()<<"?\n";
        st.pop();times--;imm.pop();
      }
      int ff=0;
      if(s=="getarray")num1+=1,out+="%"+to_string(num1)+"=",ret_num=num1,st.push("%"+to_string(num1)),imm.push(0);
      if(!haveret)num1+=1,out+="%"+to_string(num1)+"=",ret_num=num1,st.push("%"+to_string(num1)),imm.push(retn[s]);
      out+="call @"+s+"(";
      while(!rev.empty()){
        
        if(ff==1)out+=",";
        out+=rev.top();rev.pop();ff=1;
      }
      out+=")\n";
      
    }
    if(mode==3){
      int haveret=0;
      
      for(auto i=funcname.begin(); i != funcname.end(); i++){
        if((*i).first==s)haveret=(*i).second[0];
      }
      if(!haveret)num1+=1,out+="%"+to_string(num1)+"=",ret_num=num1,st.push("%"+to_string(num1)),imm.push(retn[s]);
      out+="call @"+s+"()\n";
      
    }
  }
};
class UnaryOpAST : public BaseAST {
 public:
  string op;
  void Dump(){
    string outt="";
    if(!imm.empty()){sum=imm.top();imm.pop();}
    if(op=="-"){
      num1+=1;
      outt=outt+"%"+to_string(num1)+" = sub ";
      outt+="0,"+st.top()+"\n",st.pop();
      if(flag==0)out+=outt;
      ret_num=num1;sum=-sum;st.push("%"+to_string(num1));
    }
    if(op=="!"){
      num1+=1;
      //int re=0;
      outt=outt +"%"+to_string(num1)+" = eq ";
      outt+="0,"+st.top()+"\n",st.pop();


      //tmpres=to_string(re);
      if(flag==0)out+=outt;
      ret_num=num1;sum=!sum;st.push("%"+to_string(num1));
    }
    imm.push(sum);
  }
};
class AddExpAST : public BaseAST {
 public:
  unique_ptr<BaseAST> addexp;
  unique_ptr<BaseAST> mulexp;
  string op;
  int mode;
  void Dump(){
    if(mode==0){
      mulexp->Dump();
    }
    else{
      string outt="";
      //int re=0;
      addexp->Dump();
      //if(flag==0){
      string tmp1="";if(st.size()!=0)tmp1=st.top(),st.pop();//int numt1=ret_num;
      int sum1=sum;if(!imm.empty()){sum1=imm.top();imm.pop();}
      
      mulexp->Dump();//cout<<st.size()<<endl;
      string tmp2="";if(st.size()!=0)tmp2=st.top(),st.pop();//int numt2=ret_num;
      int sum2=sum;if(!imm.empty()){sum2=imm.top();imm.pop();}
      num1+=1;
      if(op=="+"){
        outt+="%"+to_string(num1)+" = add ";sum=sum1+sum2;
      }
      if(op=="-"){
        outt+="%"+to_string(num1)+" = sub ";sum=sum1-sum2;
      }
      
      if(tmp1!="")outt+=tmp1+",";
      imm.push(sum);
      
      
      //}
      
      //if(flag==0){
      if(tmp2!="")outt+=tmp2+"\n";
      //cout<<flag<<"flag\n";
      if(flag==0)
      out+=outt;

      //ret_num=q.top();if(q.size()>1)q.pop();
      //}
      ret_num=num1;st.push("%"+to_string(num1));
      //out+="putint("+to_string(sum)+")";
    }
  }
};
class MulExpAST : public BaseAST {
 public:
  unique_ptr<BaseAST> unaryexp;
  unique_ptr<BaseAST> mulexp;
  string op;
  int mode;
  void Dump(){
    if(mode==0){unaryexp->Dump();}
    else{
      string outt="";
      //int re=0;
      
      mulexp->Dump();
      string tmp1="";if(st.size()!=0)tmp1=st.top(),st.pop();//int numt1=ret_num;
      int sum1=sum;if(!imm.empty()){sum1=imm.top();imm.pop();}
      unaryexp->Dump();
      string tmp2="";if(st.size()!=0)tmp2=st.top(),st.pop();//int numt2=ret_num;
      int sum2=sum;if(!imm.empty()){sum2=imm.top();imm.pop();}
      num1+=1;
      if(op=="*"){
        outt+="%"+to_string(num1)+" = mul ";sum=sum1*sum2;
      }
      if(op=="/"){
        outt+="%"+to_string(num1)+" = div ";if(flag==1)sum=sum1/sum2;
      }
      if(op=="%"){
        outt+="%"+to_string(num1)+" = mod ";if(flag==1)sum=sum1%sum2;
      }
      
      if(tmp1!="")outt+=tmp1+",";

      q.push(num1);
      imm.push(sum);
      
      
      if(tmp2!="")outt+=tmp2+"\n";

      if(flag==0)
      out+=outt;
      ret_num=num1;st.push("%"+to_string(num1));
      
    }
  }
};
class LOrExpAST : public BaseAST {
 public:
  unique_ptr<BaseAST> lorexp;
  unique_ptr<BaseAST> landexp;
  int mode;
  string op;
  void Dump(){
    if(mode==0){
      landexp->Dump();
    }
    else{
      string outt="";
      int x1=0;//,x2=0;
      //out+="test\n";
      lorexp->Dump();
      string tmp1="";if(st.size()!=0)tmp1=st.top(),st.pop();//int numt1=ret_num;
      int sum1=sum;if(!imm.empty()){sum1=imm.top();imm.pop();}//cout<<"lhs"<<tmp1<<endl;
      
      num1+=1;
      outt+="@duanlu_"+to_string(num1)+"= alloc i32\n";
      outt+="store 1,@duanlu_"+to_string(num1)+"\n";
      if(op=="||"){
        outt+="%"+to_string(num1)+" = eq 0,";
      }
      
      if(tmp1!="")outt+=tmp1+"\n";
      q.push(num1);
      x1=num1;
      outt+="br %"+to_string(num1)+",%then"+to_string(num1)+",%end"+to_string(num1)+"\n";
      outt+="%then"+to_string(num1)+":\n";
      if(flag==0)
      out+=outt;
      outt="";
      /*if(sum1!=0&&inmain==1){
        if(flag==0)
        out+=outt;
        st.push("%"+to_string(num1));
        imm.push(1);
        return;
      }*/
      //cout<<sum1<<"sum1\n";
      //if(sum1!=0)isfun=1;
      landexp->Dump();
      /*if(duanlu){
        if(flag==0)
        out+=outt;
        st.push("%"+to_string(num1));
        imm.push(1);duanlu=0;
        return;
      }*/
      string tmp2="";if(st.size()!=0)tmp2=st.top(),st.pop();//int numt2=ret_num;
      int sum2=sum;if(!imm.empty()){sum2=imm.top();imm.pop();}//cout<<"rhs"<<tmp2<<endl;
      num1+=1;
      outt+="%"+to_string(num1)+" = ne 0,";
      
      if(tmp2!="")outt+=tmp2+"\n";
      outt+="store %"+to_string(num1)+",@duanlu_"+to_string(x1)+"\n";
      outt+="jump %end"+to_string(x1)+"\n";
      //x2=num1;
      q.push(num1);

      num1+=1;
      outt+="%end"+to_string(x1)+":\n";
      outt+="%"+to_string(num1)+" = load @duanlu_"+to_string(x1)+"\n";
      //outt+="%"+to_string(num1)+" = or %"+to_string(x1)+",";
      //outt+="%"+to_string(x2)+"\n";
      q.push(num1);
      if(flag==0)
      out+=outt;
      ret_num=num1;sum=sum1||sum2;st.push("%"+to_string(num1));
      imm.push(sum);
    }
  }
};
class LAndExpAST : public BaseAST {
 public:
  unique_ptr<BaseAST> eqexp;
  unique_ptr<BaseAST> landexp;
  int mode;
  string op;
  void Dump(){
    if(mode==0){

      eqexp->Dump();
    }
    else{
      string outt="";
      int x1=0;//,x2=0;
      landexp->Dump();
      string tmp1="";if(st.size()!=0)tmp1=st.top(),st.pop();int numt1=ret_num;int sum1=sum;if(!imm.empty()){sum1=imm.top();imm.pop();}
      //cout<<"lhs"<<sum1<<endl;
      
      num1+=1;
      outt+="@duanlu_"+to_string(num1)+"= alloc i32\n";
      outt+="store 0,@duanlu_"+to_string(num1)+"\n";
      if(op=="&&"){
        outt+="%"+to_string(num1)+" = ne 0,";
      }
      if(tmp1!="")outt+=tmp1+"\n";
      else outt+="%"+to_string(numt1)+"\n";
      q.push(num1);
      x1=num1;
      outt+="br %"+to_string(num1)+",%then"+to_string(num1)+",%end"+to_string(num1)+"\n";
      outt+="%then"+to_string(num1)+":\n";
      if(flag==0)
      out+=outt;
      outt="";
      /*if(sum1==0&&inmain==1){cout<<"ppp\n";
        if(flag==0)
        out+=outt;
        st.push("%"+to_string(num1));
        imm.push(sum1);
        return;
      }*/
      //if(sum1==0)isfun=1;
      eqexp->Dump();
      /*if(duanlu){
        if(flag==0)
        out+=outt;
        st.push("%"+to_string(num1));
        imm.push(sum1);duanlu=0;
        return;
      }*/
      string tmp2="";if(st.size()!=0)tmp2=st.top(),st.pop();int numt2=ret_num;int sum2=sum;if(!imm.empty()){sum2=imm.top();imm.pop();}
      //cout<<"rhs"<<sum2<<endl;
      num1+=1;
      outt+="%"+to_string(num1)+" = ne 0,";
      if(tmp2!="")outt+=tmp2+"\n";
      else outt+="%"+to_string(numt2)+"\n";
      //x2=num1;
      q.push(num1);
      outt+="store %"+to_string(num1)+",@duanlu_"+to_string(x1)+"\n";
      outt+="jump %end"+to_string(x1)+"\n";

      num1+=1;
      outt+="%end"+to_string(x1)+":\n";
      outt+="%"+to_string(num1)+" = load @duanlu_"+to_string(x1)+"\n";
      //outt+="%"+to_string(num1)+" = and %"+to_string(x1)+",";
      //outt+="%"+to_string(x2)+"\n";
      q.push(num1);
      if(flag==0)
      out+=outt;
      ret_num=num1;
      sum=sum1&&sum2;st.push("%"+to_string(num1));imm.push(sum);
    }
  }
};
class EqExpAST : public BaseAST {
 public:
  unique_ptr<BaseAST> eqexp;
  unique_ptr<BaseAST> relexp;
  int mode;
  string op;
  void Dump(){
    if(mode==0){

      relexp->Dump();
    }
    else{
      string outt="";
      eqexp->Dump();
      string tmp1="";if(st.size()!=0)tmp1=st.top(),st.pop();int numt1=ret_num;int sum1=sum;if(!imm.empty()){sum1=imm.top();imm.pop();}
      relexp->Dump();
      string tmp2="";if(st.size()!=0)tmp2=st.top(),st.pop();int numt2=ret_num;int sum2=sum;if(!imm.empty()){sum2=imm.top();imm.pop();}
      num1+=1;
      if(op=="=="){
        outt+="%"+to_string(num1)+" = eq ";sum=(sum1==sum2);
      }
      if(op=="!="){
        outt+="%"+to_string(num1)+" = ne ";sum=(sum1!=sum2);
      }
      if(tmp1!="")outt+=tmp1+",";
      else outt+="%"+to_string(numt1)+",";
      q.push(num1);

      
      
      if(tmp2!="")outt+=tmp2+"\n";
      else outt+="%"+to_string(numt2)+"\n";
      if(flag==0)
      out+=outt;
      ret_num=num1;st.push("%"+to_string(num1));
      imm.push(sum);
    }
  }
};
class RelExpAST : public BaseAST {
 public:
  unique_ptr<BaseAST> relexp;
  unique_ptr<BaseAST> addexp;
  int mode;
  string op;
  void Dump(){
    if(mode==0){

      addexp->Dump();
    }
    else{
      string outt="";
      relexp->Dump();
      string tmp1="";if(st.size()!=0)tmp1=st.top(),st.pop();int numt1=ret_num;int sum1=sum;if(!imm.empty()){sum1=imm.top();imm.pop();}
      addexp->Dump();
      string tmp2="";if(st.size()!=0)tmp2=st.top(),st.pop();int numt2=ret_num;int sum2=sum;if(!imm.empty()){sum2=imm.top();imm.pop();}
      num1+=1;
      if(op=="<"){
        outt+="%"+to_string(num1)+" = lt ";sum=(sum1<sum2);
      }
      if(op==">"){
        outt+="%"+to_string(num1)+" = gt ";sum=(sum1>sum2);
      }
      if(op=="<="){
        outt+="%"+to_string(num1)+" = le ";sum=(sum1<=sum2);
      }
      if(op==">="){
        outt+="%"+to_string(num1)+" = ge ";sum=(sum1>=sum2);
      }
      if(tmp1!="")outt+=tmp1+",";
      else outt+="%"+to_string(numt1)+",";
      q.push(num1);

      
      
      if(tmp2!="")outt+=tmp2+"\n";
      else outt+="%"+to_string(numt2)+"\n";
      if(flag==0)
      out+=outt;
      ret_num=num1;st.push("%"+to_string(num1));
      imm.push(sum);
    }
  }
};
class DeclAST : public BaseAST {
 public:
  unique_ptr<BaseAST> constdecl;
  int type;
  void Dump(){
    //cout<<type<<endl;
    constdecl->Dump();
  }
};
class ConstDeclAST : public BaseAST {
 public:
  unique_ptr<BaseAST> constdefs;
  void Dump(){
    constdefs->Dump();
    
  }
};
class ConstDefsAST : public BaseAST {
 public:
  unique_ptr<BaseAST> constdef;
  unique_ptr<BaseAST> constdefs;
  int mode;
  void Dump(){
    if(mode==0)constdef->Dump();
    else{
      constdefs->Dump();
      constdef->Dump();
    }
  }
};
class ConstDefAST : public BaseAST {
 public:
  unique_ptr<BaseAST> constinitval;
  unique_ptr<BaseAST> exp;
  string s;
  int mode;
  void Dump(){
    //out+=s;
    flag=1;
    symbol_tmp.push(s);//flag=1;
    if(isglobal==0){
      
    if(mode==0){
    constinitval->Dump();
    if(imm.size()!=0)const_symbold[blockdepth][s+"_"]=to_string(imm.top()),imm.pop();
    else const_symbold[blockdepth][s+"_"]="%"+to_string(ret_num);
    //cout<<s<<const_symbold[blockdepth][s+"_"]<<endl;
    }
    if(mode==1){const_symbold[blockdepth][s+"_"]="";}
    if(mode==2){
      /*exp->Dump();
      st.pop();
      int len=imm.top();imm.pop();
      const_symbold[blockdepth][s+"_"]="-114514";
      constinitval->Dump();
      vector<int> vec;
      vector<int> vec1;
      out+="@"+s+"_"+to_string(blockdepth)+"= alloc [i32, "+to_string(len)+"]\n";
      for(int i=0;i<len;i++){
        if(!imm.empty())vec1.push_back(imm.top()),imm.pop(),st.pop();
      }
      for(int i=0;i<len;i++){
        if(i<vec1.size())
        vec.push_back(vec1[vec1.size()-i-1]);
        else vec.push_back(0);
      }
      auto ii=vec.begin();
      
      for(;ii!=vec.end();ii++){
        num1+=1;
        out+="%"+to_string(num1)+"= getelemptr @"+s+"_"+to_string(blockdepth)+","+to_string(ii-vec.begin())+"\n";
        out+="store "+to_string(*ii)+",%"+to_string(num1)+"\n";
      }
      arr[s+"_"+to_string(blockdepth)]=vec;*/
      int size1=st.size();
      exp->Dump();
      
      int len=1;
      vector<int> dy;inb.clear();
      while(st.size()>size1){
        len*=imm.top();
        dy.push_back(imm.top());imm.pop();st.pop();
      }
      const_symbold[blockdepth][s+"_"]="-114514";
      out+="@"+s+"_"+to_string(blockdepth)+"=alloc";
      string lefti[1000],righti[1000];
      for(int i=0;i<dy.size();i++){
          if(i==dy.size()-1)lefti[i]="[i32",righti[i]=","+to_string(dy[i])+"]";
          else lefti[i]="[",righti[i]=","+to_string(dy[i])+"]";
      }
      for(int i=0;i<dy.size();i++){
        out+=lefti[i];
      }
      for(int i=0;i<dy.size();i++){
        out+=righti[i];
      }
      out+="\n";
      //outt+="@"+s+"_"+to_string(blockdepth)+"= alloc [i32, "+to_string(len)+"]\n";
      //int kn=ik;
      for(int i=0;i<dy.size()-1;i++){
        int nn=1;
        for(int j=0;j<=i;j++){
          nn*=dy[j];
        }
        inb.push_back(nn);
      }
      arrsize[s+"_"+to_string(blockdepth)]=inb;
      int iss=imm.size();
      constinitval->Dump();
      vector<int> vec;
      vector<int> vec1;
      for(int i=0;i<len;i++){
        if(imm.size()>iss){
          vec1.push_back(imm.top()),imm.pop();
          //cout<<"size"<<st.size()<<imm.size()<<endl;
          st.pop();
        }
      }
      for(int i=0;i<len;i++){
        if(i<vec1.size())
        vec.push_back(vec1[vec1.size()-i-1]);
        else vec.push_back(0);
      }//cout<<vec.size()<<"aaasd\n";
      arr[s+"_"+to_string(blockdepth)]=vec;
      auto ii=vec.begin();
      
      /*for(;ii!=vec.end();ii++){
        num1+=1;
        outt+="%"+to_string(num1)+"= getelemptr @"+s+"_"+to_string(blockdepth)+","+to_string(ii-vec.begin())+"\n";
        outt+="store "+to_string(*ii)+",%"+to_string(num1)+"\n";
      }*/
      vector<vector<int>> uu;
      map<int,vector<int>> ceng;
      for(int i=0;i<vec.size();i++){
        for(int jj=inb.size()- 1; jj >= 0; jj--){
          vector<int> tmp1;vector<int> tmp2;ceng[jj]=tmp2;
          if((i+1)%inb[jj]==1){
            //cout<<i<<" "<<inb[jj]<<"test\n";
            tmp1.push_back(i),tmp1.push_back(jj),uu.push_back(tmp1);
            
          }
        }
      }
      stack<pair<int,int>> nn1;
      
      for(;ii!=vec.end();ii++){
        //cout<<uu[jj][0]<<"op\n";
        int flag2=1;
        for(int jj=0;jj<uu.size();jj++){
        if(uu.size()>0&&(ii-vec.begin())==uu[jj][0]){
          num1+=1;
          //cout<<uu[jj][0]<<" "<<uu[jj][1]<<" "<<inb[inb.size()-1]<<"ll"<<endl;
          if(inb[uu[jj][1]]==inb[inb.size()-1]){
            int k=0;
              while(k<uu[jj][1]){
                ceng[k].clear();k++;
              }
            out+="%"+to_string(num1)+"= getelemptr @"+s+"_"+to_string(blockdepth)+","+to_string((ii-vec.begin())/inb[inb.size()-1])+"\n";
            auto pa=make_pair(num1,uu[jj][1]);
            nn1.push(pa);
            
          }
            else{
              //int tmp3=0;if(uu[jj][1]==nn1.top().second)tmp3=1;
              //cout<<ceng[uu[jj][1]+1].size()<<endl;
              int k=0;
              while(k<uu[jj][1]){
                ceng[k].clear();k++;
              }
              auto iii=ceng[uu[jj][1]+1].end()-1;
            out+="%"+to_string(num1)+"= getelemptr %"+to_string(*iii)+","+to_string(ceng[uu[jj][1]].size())+"\n";
            nn1.pop();
            if(uu[jj][1]!=flag2){
              auto pa=make_pair(num1,uu[jj][1]);
              nn1.push(pa);
            }
            }
            flag2=uu[jj][1];
            ceng[uu[jj][1]].push_back(num1);
          
        }
        }
        num1+=1;
        if(inb.size()==0)out+="%"+to_string(num1)+"= getelemptr @"+s+"_"+to_string(blockdepth)+","+to_string(ii-vec.begin())+"\n";
        else
            out+="%"+to_string(num1)+"= getelemptr %"+to_string(nn1.top().first)+","+to_string((ii-vec.begin())%inb[0])+"\n";
        out+="store "+to_string(*ii)+",%"+to_string(num1)+"\n";
      }
    }
    flag=0;
    }
    else{flag=1;
      if(mode==0)constinitval->Dump(),const_symbold[0][s+"_"]=to_string(imm.top()),imm.pop();
      if(mode==1) const_symbold[0][s+"_"]="";
      if(mode==2){
        /*exp->Dump();
      st.pop();
      int len=imm.top();imm.pop();
      const_symbold[0][s+"_"]="-114514";
      constinitval->Dump();
      vector<int> vec;
      vector<int> vec1;
      out+="global @"+s+"_0 = alloc [i32, "+to_string(len)+"]";
      for(int i=0;i<len;i++){
        if(!imm.empty())vec1.push_back(imm.top()),imm.pop(),st.pop();
      }
      for(int i=0;i<len;i++){
        if(i<vec1.size())
        vec.push_back(vec1[vec1.size()-i-1]);
        else vec.push_back(0);
      }
      auto ii=vec.begin();
      out+=",{";
      for(;ii!=vec.end();ii++){

        out+=to_string(*ii);
        if(ii+1!=vec.end())out+=",";
      }
      out+="}\n";
      arr[s+"_"+to_string(0)]=vec;*/
      int size1=st.size();
      exp->Dump();
      
      int len=1;
      vector<int> dy;
      inb.clear();
      while(st.size()>size1){
        len*=imm.top();//cout<<imm.size()<<" "<<st.size()<<endl;
        dy.push_back(imm.top());imm.pop();st.pop();
      }//cout<<"test2\n";
      const_symbold[0][s+"_"]="-114514";
      out+="global @"+s+"_"+to_string(0)+"=alloc";
      string lefti[1000],righti[1000];
      for(int i=0;i<dy.size();i++){
          if(i==dy.size()-1)lefti[i]="[i32",righti[i]=","+to_string(dy[i])+"]";
          else lefti[i]="[",righti[i]=","+to_string(dy[i])+"]";
      }
      for(int i=0;i<dy.size();i++){
        out+=lefti[i];
      }
      for(int i=0;i<dy.size();i++){
        out+=righti[i];
      }
      for(int i=0;i<dy.size()-1;i++){
        int nn=1;
        for(int j=0;j<=i;j++){
          nn*=dy[j];
        }
        inb.push_back(nn);
      }
      arrsize[s+"_"+to_string(0)]=inb;
      int iss=imm.size();
      constinitval->Dump();
      vector<int> vec;
      vector<int> vec1;
      for(int i=0;i<len;i++){
        if(imm.size()>iss){
          vec1.push_back(imm.top()),imm.pop();
          st.pop();
        }
      }
      for(int i=0;i<len;i++){
        if(i<vec1.size())
        vec.push_back(vec1[vec1.size()-i-1]);
        else vec.push_back(0);
      }
      arr[s+"_"+to_string(0)]=vec;
      auto ii=vec.begin();
      
      out+=",{";
      string outt1="",outt2="";
      for(;ii!=vec.end();ii++){
        //int flag2=0;
        for(int jj=inb.size()-1;jj>=0;jj--){
          if((ii-vec.begin()+1)%inb[jj]==1){
            
            if(ii==vec.begin())out+="{";
            else if(jj+1<inb.size()&&(ii-vec.begin())%inb[jj+1]==0)out+="{";
            else out+=",{";
          }
          //if((ii-vec.begin()+1)%inb[jj]==0)cout<<ii-vec.begin()<<" "<<inb[jj]<<"ooo"<<endl,flag2+=1;
        }
        //if(flag2>1||ii==vec.begin())outt+="{";
        out+=to_string(*ii);
        int flag2=0;
        for(int jj=inb.size()-1;jj>=0;jj--){
          if((ii-vec.begin()+1)%inb[jj]==0){
            flag2=1;
            out+="}";
            //else outt+=",{";
          }
          //else outt+=",";
        }
        if(flag2==0&&ii!=vec.end()-1)out+=",";
        /*cout<<flag2<<"flag2\n";
        if(flag2==0||ii==vec.begin())outt+=",";
        else if(ii!=vec.begin())outt+="}";
        if(flag2==1&&ii!=vec.begin()&&ii!=vec.end()-1)outt+=",{";*/
        
      }
      out+="}\n";
      }
      //flag=0;
    }
    
  }
};
class ValDefAST : public BaseAST {
 public:
  unique_ptr<BaseAST> constinitval;
  unique_ptr<BaseAST> exp;
  string s;
  int mode;
  void Dump(){
    //out+=s;
    string outt="";
    
    symbol_tmp.push(s);
    if(isglobal==0){
    
    
    if(mode==0){
      int ex=0;
    if(find(val.begin(),val.end(),s+to_string(blockdepth))==val.end())val.push_back(s+to_string(blockdepth)),ex=1;
    //if(symbold[blockdepth].find(s+"_")==symbold[blockdepth].end())val.push_back(s+"_"+to_string(blockdepth)),ex=1;
    if(ex)
    outt+="@"+s+"_"+to_string(blockdepth)+" = alloc i32\n";
    constinitval->Dump();
    
    
    if(st.size()!=0){
      symbold[blockdepth][s+"_"]=st.top();outt+="store "+st.top()+",@"+s+"_"+to_string(blockdepth)+"\n";
    if(st.top()[0]=='%')line["%"+to_string(ret_num)]=imm.top();
    st.pop();imm.pop();
    }
    else symbold[blockdepth][s+"_"]="%"+to_string(ret_num),outt+="store %"+to_string(ret_num)+",@"+s+"_"+to_string(blockdepth)+"\n",line["%"+to_string(ret_num)]=imm.top(),imm.pop();
    
    }
    if(mode==1){
      int ex=0;
    if(find(val.begin(),val.end(),s+to_string(blockdepth))==val.end())val.push_back(s+to_string(blockdepth)),ex=1;
    if(ex)
    outt+="@"+s+"_"+to_string(blockdepth)+" = alloc i32\n";
      symbold[blockdepth][s+"_"] = "0";
    }
    if(mode==2){
      int size1=st.size();
      exp->Dump();
      
      int len=1;
      vector<int> dy;inb.clear();
      while(st.size()>size1){
        len*=imm.top();//cout<<imm.size()<<" "<<st.size();cout<<" "<<imm.top()<<endl;//<<" "<<st.top()<<endl;
        dy.push_back(imm.top());imm.pop();st.pop();
      }
      symbold[blockdepth][s+"_"]="-114514";
      outt+="@"+s+"_"+to_string(blockdepth)+"=alloc";
      string lefti[1000],righti[1000];
      for(int i=0;i<dy.size();i++){
          if(i==dy.size()-1)lefti[i]="[i32",righti[i]=","+to_string(dy[i])+"]";
          else lefti[i]="[",righti[i]=","+to_string(dy[i])+"]";
      }
      for(int i=0;i<dy.size();i++){
        outt+=lefti[i];
      }
      for(int i=0;i<dy.size();i++){
        outt+=righti[i];
      }
      outt+="\n";
      //outt+="@"+s+"_"+to_string(blockdepth)+"= alloc [i32, "+to_string(len)+"]\n";
      //int kn=ik;
      for(int i=0;i<dy.size()-1;i++){
        int nn=1;
        for(int j=0;j<=i;j++){
          nn*=dy[j];
        }
        inb.push_back(nn);
      }
      arrsize[s+"_"+to_string(blockdepth)]=inb;
      int iss=imm.size();
      constinitval->Dump();
      vector<int> vec;
      vector<int> vec1;
      for(int i=0;i<len;i++){
        if(imm.size()>iss){
          vec1.push_back(imm.top()),imm.pop();
          //cout<<"size"<<st.size()<<imm.size()<<endl;
          st.pop();
        }
      }
      for(int i=0;i<len;i++){
        if(i<vec1.size())
        vec.push_back(vec1[vec1.size()-i-1]);
        else vec.push_back(0);
      }//cout<<vec.size()<<"aaasd\n";
      arr[s+"_"+to_string(blockdepth)]=vec;
      auto ii=vec.begin();
      
      /*for(;ii!=vec.end();ii++){
        num1+=1;
        outt+="%"+to_string(num1)+"= getelemptr @"+s+"_"+to_string(blockdepth)+","+to_string(ii-vec.begin())+"\n";
        outt+="store "+to_string(*ii)+",%"+to_string(num1)+"\n";
      }*/
      vector<vector<int>> uu;
      map<int,vector<int>> ceng;
      for(int i=0;i<vec.size();i++){
        for(int jj=inb.size()- 1; jj >= 0; jj--){
          vector<int> tmp1;vector<int> tmp2;ceng[jj]=tmp2;
          if((i+1)%inb[jj]==1){
            //cout<<i<<" "<<inb[jj]<<"test\n";
            tmp1.push_back(i),tmp1.push_back(jj),uu.push_back(tmp1);
            
          }
        }
      }
      stack<pair<int,int>> nn1;
      
      for(;ii!=vec.end();ii++){
        //cout<<uu[jj][0]<<"op\n";
        int flag2=1;
        for(int jj=0;jj<uu.size();jj++){
        if(uu.size()>0&&(ii-vec.begin())==uu[jj][0]){
          num1+=1;
          //cout<<uu[jj][0]<<" "<<uu[jj][1]<<" "<<inb[inb.size()-1]<<endl;
          if(inb[uu[jj][1]]==inb[inb.size()-1]){
            int k=0;
              while(k<uu[jj][1]){
                ceng[k].clear();k++;
              }
            outt+="%"+to_string(num1)+"= getelemptr @"+s+"_"+to_string(blockdepth)+","+to_string((ii-vec.begin())/inb[inb.size()-1])+"\n";
            auto pa=make_pair(num1,uu[jj][1]);
            nn1.push(pa);
            
          }
            else{
              //int tmp3=0;if(uu[jj][1]==nn1.top().second)tmp3=1;
              //cout<<ceng[uu[jj][1]+1].size()<<endl;
              int k=0;
              while(k<uu[jj][1]){
                ceng[k].clear();k++;
              }
              auto iii=ceng[uu[jj][1]+1].end()-1;
            outt+="%"+to_string(num1)+"= getelemptr %"+to_string(*iii)+","+to_string(ceng[uu[jj][1]].size())+"\n";
            nn1.pop();
            if(uu[jj][1]!=flag2){
              auto pa=make_pair(num1,uu[jj][1]);
              nn1.push(pa);
            }
            }
            flag2=uu[jj][1];
            ceng[uu[jj][1]].push_back(num1);
          
        }
        }
        num1+=1;
        if(inb.size()==0)outt+="%"+to_string(num1)+"= getelemptr @"+s+"_"+to_string(blockdepth)+","+to_string(ii-vec.begin())+"\n";
        else
            outt+="%"+to_string(num1)+"= getelemptr %"+to_string(nn1.top().first)+","+to_string((ii-vec.begin())%inb[0])+"\n";
        outt+="store "+to_string(*ii)+",%"+to_string(num1)+"\n";
      }
      /*for(;ii!=vec.end();ii++){
        int flag2=0;
        for(int jj=inb.size()- 1; jj >= 0; jj--){
          
          if((ii-vec.begin()+1)%inb[jj]==1){cout<<ii-vec.begin()<<" "<<inb[jj]<<"Asf"<<endl;
            num1+=1;
            if(flag2==0)
            outt+="%"+to_string(num1)+"= getelemptr @"+s+"_"+to_string(blockdepth)+","+to_string((ii-vec.begin())/inb[jj])+"\n";
            else
            outt+="%"+to_string(num1)+"= getelemptr %"+to_string(num1-1)+","+to_string((ii-vec.begin())/inb[jj])+"\n";
            flag2=1;
        //outt+="store "+to_string(*ii)+",%"+to_string(num1)+"\n";
          }
          
        }
        num1+=1;
        if(inb.size()==0)outt+="%"+to_string(num1)+"= getelemptr @"+s+"_"+to_string(blockdepth)+","+to_string(ii-vec.begin())+"\n";
        else
            outt+="%"+to_string(num1)+"= getelemptr %"+to_string(num1-1)+","+to_string((ii-vec.begin())%inb[0])+"\n";
        outt+="store "+to_string(*ii)+",%"+to_string(num1)+"\n";
      }*/
      
    }
    if(mode==3){
      /*constinitval->Dump();st.pop();
      int len=imm.top();imm.pop();
      symbold[blockdepth][s+"_"]="-114514";
      outt+="@"+s+"_"+to_string(blockdepth)+"= alloc [i32, "+to_string(len)+"]\n";
      vector<int> vec;
      for(int i=0;i<len;i++){
        vec.push_back(0);
      }
      arr[s+"_"+to_string(blockdepth)]=vec;*/

      int size1=st.size();
      constinitval->Dump();
      
      int len=1;
      vector<int> dy;inb.clear();
      while(st.size()>size1){
        len*=imm.top();
        dy.push_back(imm.top());imm.pop();st.pop();
      }
      symbold[blockdepth][s+"_"]="-114514";
      outt+="@"+s+"_"+to_string(blockdepth)+"=alloc";
      string lefti[1000],righti[1000];
      for(int i=0;i<dy.size();i++){
          if(i==dy.size()-1)lefti[i]="[i32",righti[i]=","+to_string(dy[i])+"]";
          else lefti[i]="[",righti[i]=","+to_string(dy[i])+"]";
      }
      for(int i=0;i<dy.size();i++){
        outt+=lefti[i];
      }
      for(int i=0;i<dy.size();i++){
        outt+=righti[i];
      }
      outt+="\n";
      //outt+="@"+s+"_"+to_string(blockdepth)+"= alloc [i32, "+to_string(len)+"]\n";
      //int kn=ik;
      for(int i=0;i<dy.size()-1;i++){
        int nn=1;
        for(int j=0;j<=i;j++){
          nn*=dy[j];
        }
        inb.push_back(nn);
      }
      arrsize[s+"_"+to_string(blockdepth)]=inb;
      vector<int> vec;
      for(int i=0;i<len;i++){
        vec.push_back(0);
      }
      arr[s+"_"+to_string(blockdepth)]=vec;
    }
    }
    else{//cout<<s<<endl;
     
      if(mode==0){outt+="global @"+s+"_0"+" = alloc i32,";
      constinitval->Dump();symbold[0][s+"_"]=to_string(imm.top());outt+=to_string(imm.top())+"\n";imm.pop();
      }
      if(mode==1){outt+="global @"+s+"_0"+" = alloc i32,";symbold[0][s+"_"] = "0",outt+="zeroinit\n"; }
      if(mode==2){
        /*exp->Dump();
      st.pop();
      int len=imm.top();imm.pop();
      symbold[0][s+"_"]="-114514";
      outt+="global @"+s+"_0 = alloc [i32, "+to_string(len)+"]";
      constinitval->Dump();
      vector<int> vec;
      vector<int> vec1;
      for(int i=0;i<len;i++){
        if(!imm.empty())vec1.push_back(imm.top()),imm.pop(),st.pop();
      }
      for(int i=0;i<len;i++){
        if(i<vec1.size())
        vec.push_back(vec1[vec1.size()-i-1]);
        else vec.push_back(0);
      }
      auto ii=vec.begin();
      outt+=",{";
      for(;ii!=vec.end();ii++){

        outt+=to_string(*ii);
        if(ii+1!=vec.end())outt+=",";
      }
      outt+="}\n";
      arr[s+"_"+to_string(0)]=vec;*/
      int size1=st.size();
      exp->Dump();
      
      int len=1;
      vector<int> dy;
      inb.clear();
      while(st.size()>size1){
        len*=imm.top();//cout<<imm.size()<<" "<<st.size()<<endl;
        dy.push_back(imm.top());imm.pop();st.pop();
      }//cout<<"test2\n";
      symbold[0][s+"_"]="-114514";
      outt+="global @"+s+"_"+to_string(0)+"=alloc";
      string lefti[1000],righti[1000];
      for(int i=0;i<dy.size();i++){
          if(i==dy.size()-1)lefti[i]="[i32",righti[i]=","+to_string(dy[i])+"]";
          else lefti[i]="[",righti[i]=","+to_string(dy[i])+"]";
      }
      for(int i=0;i<dy.size();i++){
        outt+=lefti[i];
      }
      for(int i=0;i<dy.size();i++){
        outt+=righti[i];
      }
      for(int i=0;i<dy.size()-1;i++){
        int nn=1;
        for(int j=0;j<=i;j++){
          nn*=dy[j];
        }
        inb.push_back(nn);
      }
      arrsize[s+"_"+to_string(0)]=inb;
      int iss=imm.size();
      constinitval->Dump();
      vector<int> vec;
      vector<int> vec1;
      for(int i=0;i<len;i++){
        if(imm.size()>iss){
          vec1.push_back(imm.top()),imm.pop();
          st.pop();
        }
      }
      for(int i=0;i<len;i++){
        if(i<vec1.size())
        vec.push_back(vec1[vec1.size()-i-1]);
        else vec.push_back(0);
      }
      arr[s+"_"+to_string(0)]=vec;
      auto ii=vec.begin();
      
      outt+=",{";
      string outt1="",outt2="";
      for(;ii!=vec.end();ii++){
        //int flag2=0;
        for(int jj=inb.size()-1;jj>=0;jj--){
          if((ii-vec.begin()+1)%inb[jj]==1){
            
            if(ii==vec.begin())outt+="{";
            else if(jj+1<inb.size()&&(ii-vec.begin())%inb[jj+1]==0)outt+="{";
            else outt+=",{";
          }
          //if((ii-vec.begin()+1)%inb[jj]==0)cout<<ii-vec.begin()<<" "<<inb[jj]<<"ooo"<<endl,flag2+=1;
        }
        //if(flag2>1||ii==vec.begin())outt+="{";
        outt+=to_string(*ii);
        int flag2=0;
        for(int jj=inb.size()-1;jj>=0;jj--){
          if((ii-vec.begin()+1)%inb[jj]==0){
            flag2=1;
            outt+="}";
            //else outt+=",{";
          }
          //else outt+=",";
        }
        if(flag2==0&&ii!=vec.end()-1)outt+=",";
        /*cout<<flag2<<"flag2\n";
        if(flag2==0||ii==vec.begin())outt+=",";
        else if(ii!=vec.begin())outt+="}";
        if(flag2==1&&ii!=vec.begin()&&ii!=vec.end()-1)outt+=",{";*/
        
      }
      outt+="}\n";

      }
      if(mode==3){
        /*constinitval->Dump();st.pop();
      int len=imm.top();imm.pop();
      symbold[0][s+"_"]="-114514";
      outt+="global @"+s+"_0 = alloc [i32, "+to_string(len)+"]";
      vector<int> vec;
      for(int i=0;i<len;i++){
        vec.push_back(0);
      }
      outt+=",zeroinit\n";
      arr[s+"_"+to_string(0)]=vec;*/

      int size1=st.size();
      constinitval->Dump();
      
      int len=1;
      vector<int> dy;inb.clear();
      while(st.size()>size1){
        len*=imm.top();
        dy.push_back(imm.top());imm.pop();st.pop();
      }
      symbold[0][s+"_"]="-114514";
      outt+="global @"+s+"_"+to_string(0)+"=alloc";
      string lefti[1000],righti[1000];
      for(int i=0;i<dy.size();i++){
          if(i==dy.size()-1)lefti[i]="[i32",righti[i]=","+to_string(dy[i])+"]";
          else lefti[i]="[",righti[i]=","+to_string(dy[i])+"]";
      }
      for(int i=0;i<dy.size();i++){
        outt+=lefti[i];
      }
      for(int i=0;i<dy.size();i++){
        outt+=righti[i];
      }
       outt+=",zeroinit\n";
      //outt+="@"+s+"_"+to_string(blockdepth)+"= alloc [i32, "+to_string(len)+"]\n";
      //int kn=ik;
      for(int i=0;i<dy.size()-1;i++){
        int nn=1;
        for(int j=0;j<=i;j++){
          nn*=dy[j];
        }
        inb.push_back(nn);
      }
      arrsize[s+"_"+to_string(0)]=inb;
      vector<int> vec;
      for(int i=0;i<len;i++){
        vec.push_back(0);
      }
      arr[s+"_"+to_string(0)]=vec;
      cout<<s+"_"+to_string(0)<<"--------"<<arr[s+"_"+to_string(0)].size()<<" "<<endl;
      }
    }
    out+=outt;
  }
};
class VarDeclAST : public BaseAST {
 public:
  unique_ptr<BaseAST> constdefs;
  void Dump(){
    constdefs->Dump();
    
  }
};
class VarDefsAST : public BaseAST {
 public:
  unique_ptr<BaseAST> constdef;
  unique_ptr<BaseAST> constdefs;
  int mode;
  void Dump(){
    if(mode==0)constdef->Dump();
    else{
      constdefs->Dump();
      constdef->Dump();
    }
  }
};

class ConstInitValAST : public BaseAST {
 public:
  unique_ptr<BaseAST> constexp;
  int mode;
  void Dump(){
    if(mode==0){
    constexp->Dump();
    }
    if(mode==1){
      
      ik+=1;
      int len1=imm.size();
      for(int i=0;i<inb.size();i++){
        if(imm.size()%inb[i]!=0)break;
        len1=inb[i];
      }
      
      constexp->Dump();
      int len2=imm.size();
      //cout<<len1<<" "<<len2<<"len\n";
      int times1=0;
      if(len1!=0){
      while(times1*len1-len2<0){
        times1+=1;
      }
      for(int i=0;i<times1*len1-len2;i++){
        imm.push(0);st.push(to_string(0));
      }
      times1++;
      ik-=1;
      }
    }
    if(mode==2){
      int len=imm.size();
      for(int i=0;i<inb.size();i++){
        if(imm.size()%inb[i]!=0)break;
        len=imm.size();
      }
      for(int i=0;i<len;i++){
        imm.push(0);st.push(to_string(0));
      }
    }
    
  }
};
class BlockItemAST : public BaseAST {
 public:
  unique_ptr<BaseAST> decl;
  unique_ptr<BaseAST> stmt;
  unique_ptr<BaseAST> blockitem;
  int mode;
  void Dump(){
    //cout<<mode<<"blockitem\n";
    if(rr&&flag1==0){return;}
    if(mode==0){
      decl->Dump();if(rr&&flag1==0){return;}
    }
    else if(mode==1){
      stmt->Dump();if(rr&&flag1==0){return;}
    }
    else if(mode==2){blockitem->Dump();if(rr&&flag1==0){return;};decl->Dump();}
    else if(mode==3){blockitem->Dump();if(rr&&flag1==0){return;};stmt->Dump();}
  }
};
class ConstExpAST : public BaseAST {
 public:
  unique_ptr<BaseAST> exp;
  void Dump(){
    exp->Dump();

  }
};
class ConstExp1AST : public BaseAST {
 public:
  unique_ptr<BaseAST> exp1;
  unique_ptr<BaseAST> exp2;
  int mode;
  void Dump(){
    if(mode==0){
      exp1->Dump();
      exp2->Dump();
    }
    if(mode==1){
      exp1->Dump();
    }
  }
};
class ConstExp2AST : public BaseAST {
 public:
  unique_ptr<BaseAST> exp1;
  unique_ptr<BaseAST> exp2;
  int mode;
  void Dump(){
    if(mode==0){
      exp1->Dump();
      exp2->Dump();
    }
    if(mode==1){
      exp1->Dump();
    }
  }
};
class ExpsAST : public BaseAST {
 public:
  unique_ptr<BaseAST> exp1;
  unique_ptr<BaseAST> exp2;
  int mode;
  void Dump(){
    if(mode==0){
      exp1->Dump();
      exp2->Dump();
    }
    if(mode==1){
      exp1->Dump();
    }
  }
};
class ExpssAST : public BaseAST {
 public:
  unique_ptr<BaseAST> exp1;
  unique_ptr<BaseAST> exp2;
  int mode;
  void Dump(){
    if(mode==0){
      exp1->Dump();
      exp2->Dump();
    }
    if(mode==1){
      exp1->Dump();
    }
  }
};