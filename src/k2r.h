#include <cassert>
#include <cstdio>
#include <iostream>
#include <memory>
#include <string>
#include <map>
#include<vector>
#include "koopa.h"

#include<typeinfo>
#include <stack>
#include <algorithm>

using namespace std;
static string riscv="";
void Visit(const koopa_raw_program_t &program);
void Visit(const koopa_raw_slice_t &slice);
void Visit(const koopa_raw_function_t &func);
void Visit(const koopa_raw_basic_block_t &bb);
void Visit(const koopa_raw_value_t &value);
void Visit(const koopa_raw_return_t &ret);
void Visit(const koopa_raw_integer_t &in);
void Visit(const koopa_raw_binary_t &bin);
void Visit(const koopa_raw_binary_op_t &op);
void Visit(const koopa_raw_store_t &store);
void Visit(const koopa_raw_load_t &load);
void Visit(const koopa_raw_branch_t &branch);
void Visit(const koopa_raw_jump_t &jump);
void Visit(const koopa_raw_global_alloc_t&global_alloc);
void Visit(const koopa_raw_call_t &call);
void Visit(const koopa_raw_func_arg_ref_t&func_arg_ref);
void Visit(const koopa_raw_get_ptr_t&g);
void Visit(const koopa_raw_get_elem_ptr_t &g);
void Visit(const koopa_raw_aggregate_t&a);
static string reg[15]={"t0","t1","t2","t3","t4","t5","t6","a0","a1","a2","a3","a4","a5","a6","a7"};
static string libfunc[8]={"getint","getch","getarray","putint","putch","putarray","starttime","stoptime"};
static map<koopa_raw_value_t,int> symbol1;
static map<string, int> alloc_pos;
//static int reg_num=0;
static string tmp="";
//static int used=0;
int used[15] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
static int ret_num1=0;
static stack<int> stack1;
static stack<string> braname;
static map<string,int> alloc_flag;
static map<string,vector<int>> alloc_size;
static vector<int> sizea;
static vector<int> change;
static vector<int> change1;
static int callp=0;
static int use=0;
static int sp_max=0;
static int isbra=0;
static int lhs=0;
static int fp=0;
static int global=0;
static int zeroinit=0;
// 访问 raw program
void Visit(const koopa_raw_program_t &program) {
  // 执行一些其他的必要操作
  // ...
  // 访问所有全局变量
  
  Visit(program.values);
  // 访问所有函数
  Visit(program.funcs);
}

// 访问 raw slice
void Visit(const koopa_raw_slice_t &slice) {
  int tmp1=-40960;
  for (size_t i = 0; i < slice.len; ++i) {
    auto ptr = slice.buffer[i];
    // 根据 slice 的 kind 决定将 ptr 视作何种元素
    switch (slice.kind) {
      case KOOPA_RSIK_FUNCTION:
        // 访问函数
        Visit(reinterpret_cast<koopa_raw_function_t>(ptr));
        break;
      case KOOPA_RSIK_BASIC_BLOCK:
        // 访问基本块
        Visit(reinterpret_cast<koopa_raw_basic_block_t>(ptr));
        break;
      case KOOPA_RSIK_VALUE:{
        // 访问指令
        int si1=stack1.size();
        
        Visit(reinterpret_cast<koopa_raw_value_t>(ptr));
        if(callp==1){
          int si2=stack1.size();
          vector<int> fn;
          while(fn.size()<si2-si1)fn.push_back(stack1.top()),stack1.pop();
          int fni=fn.size()-1;
          if(i<8){
          
            //s1.erase(s1.begin());
            
            riscv+="add a"+to_string(i)+","+reg[fn[fni]]+",zero\n";fni--;
            used[fn[fni]]=0;
            //riscv+="sw a"+to_string(i)+","+to_string(sp_max)+"(sp)\n";
          
          }
          else{
            
            

              //s1.erase(s1.begin());
              int f = 0;
                for(int j = 0 ; j <15; j++){ 
                    if(used[j] == 0){
                        //used[j] = 1;
                        f= j;break;
                    }
                }
            riscv+="li "+reg[f]+","+to_string(tmp1)+"\n";
              riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
              riscv+="sw "+reg[fn[fni]]+",0("+reg[f]+")\n";fni--;used[fn[fni]]=0;tmp1+=4;
            //riscv+="lw "+reg[f]+","+to_string(tmp1)+"(sp)\n";
            /*if(sp_max<=1024)
              riscv+="sw "+reg[f]+","+to_string(sp_max)+"(sp)\n";
              else{
                int f1 = 0;
        for(int j = 0 ; j <15; j++){ 
            if(used[j] == 0&&j!=f){
                f1= j;break;
            }
        }
        riscv+="li "+reg[f1]+","+to_string(sp_max)+"\n";
        riscv+="add "+reg[f1]+",sp,"+reg[f1]+"\n";
        riscv+="sw "+reg[f]+",0("+reg[f1]+")\n";
              }
              change1.push_back(tmp1);
              change.push_back(sp_max);used[fn[fni]]=0;
              riscv+="li "+reg[f]+","+to_string(tmp1)+"\n";
              riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
              riscv+="sw "+reg[fn[fni]]+",0("+reg[f]+")\n";fni--;
              tmp1+=4,sp_max+=4;*/
          }
        }
        break;
      }
      default:
        // 我们暂时不会遇到其他内容, 于是不对其做任何处理
        assert(false);
    }
  }
}

// 访问函数
void Visit(const koopa_raw_function_t &func) {
  // 执行一些其他的必要操作
  // ...
  // 访问所有基本块
  
  string s1=func->name;
  //int mapsize=alloc_pos.size();
  auto tmpmap=alloc_pos;
  s1.erase(s1.begin());
  int flag1=0;
  for(int i=0;i<8;i++)if(libfunc[i]==s1){flag1=1;break;}
  if(flag1==0){
    riscv+="  .text\n  .globl ";
  riscv=riscv+s1;
  riscv=riscv+"\n";
  riscv=riscv+s1;
  riscv=riscv+":\n";
  riscv+="li t0,-40960\n";
  riscv+="add sp,sp,t0\n";
  //riscv+="sw ra,0(sp)\n";
  //int tmp1=func->params.len>8?func->params.len:8;
    riscv+="li t0,40956\n";
  riscv+="add t0,sp,t0\n";
  riscv+="sw ra,0(t0)\n";
 // riscv+="sw ra,"+to_string(-40956)+"(sp)\n";
  
  sp_max=0;
  if(func->params.len<=8){
  for(int i=0;i<func->params.len;i++){
    auto ptr = func->params.buffer[i];
    string s1=reinterpret_cast<koopa_raw_value_t>(ptr)->name;
    //s1.erase(s1.begin());
    alloc_pos[s1]=sp_max;
    alloc_flag[s1]=1;
    riscv+="sw a"+to_string(i)+","+to_string(sp_max)+"(sp)\n";sp_max+=4;
  }
  }
  else{
    sp_max+=(func->params.len-8)*4;
    for(int i=0;i<8;i++){
      auto ptr = func->params.buffer[i];
      string s1=reinterpret_cast<koopa_raw_value_t>(ptr)->name;
      //auto value=reinterpret_cast<koopa_raw_value_t>(ptr);
      
      //s1.erase(s1.begin());
      alloc_pos[s1]=sp_max;alloc_flag[s1]=1;
      riscv+="sw a"+to_string(i)+","+to_string(sp_max)+"(sp)\n";sp_max+=4;
    }
    for(int i=0;i<func->params.len-8;i++){
      auto ptr = func->params.buffer[i+8];
      string s1=reinterpret_cast<koopa_raw_value_t>(ptr)->name;
      //s1.erase(s1.begin());
      alloc_pos[s1]=sp_max;alloc_flag[s1]=1;
      int f = 0;
                for(int j = 0 ; j <15; j++){ 
                    if(used[j] == 0){
                        //used[j] = 1;
                        f= j;break;
                    }
                }
                //riscv+="li "+reg[f]+","+to_string(4*i+40960)+"\n";
                //riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
                //riscv+="lw "+reg[f]+",0("+reg[f]+")\n";
            riscv+="lw "+reg[f]+","+to_string(4*i)+"(sp)\n";
      riscv+="sw "+reg[f]+","+to_string(sp_max)+"(sp)\n";
      sp_max+=4;
    }
  }
  //Visit(func->params);
  //for(int i=0;)
  
  Visit(func->bbs);
  /*auto ii=alloc_pos.begin();
  while(distance(alloc_pos.begin(),ii)<=mapsize)ii++;
  vector<string> del;
  for(;ii!=alloc_pos.end();ii++){
    del.push_back((*ii).first);
  }
  for(int i=0;i<del.size();i++){
    //riscv+=del[i]+"\n";
    cout<<del[i]<<" "<<mapsize<<"del\n";
    alloc_pos.erase(del[i]);
    alloc_flag.erase(del[i]);
  }*/
  alloc_pos.clear();
  alloc_pos=tmpmap;
  }
  
}

// 访问基本块
void Visit(const koopa_raw_basic_block_t &bb) {
  // 执行一些其他的必要操作
  // ...
  // 访问所有指令
  string s1=bb->name;
  s1.erase(s1.begin());
  braname.push(s1);
  riscv+=s1+":\n";
  Visit(bb->insts);
  //Visit(bb->params);
}

// 访问指令
void Visit(const koopa_raw_value_t &value) {
  //cout<<"\n?"<<value->kind.tag<<endl;
  //cout<<endl<<riscv<<endl;
  
  if(symbol1.find(value) != symbol1.end()){
    int f = 0;
      for(int i=0;i<15;i++){
          if(used[i] == 0){
              used[i] = 1;f=i;
              break;
          }
      }
      if(symbol1[value]<0){
        riscv+="la "+reg[f]+","+string(value->name)+"\n";
        riscv+="lw "+reg[f]+",0("+reg[f]+")\n";
      }
      else{
      if(sp_max<=1024){
        riscv+="lw "+reg[f]+","+to_string(symbol1[value])+"(sp)\n";
      }
      else{
        riscv+="li "+reg[f]+","+to_string(symbol1[value])+"\n";
        riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
        riscv+="lw "+reg[f]+",0("+reg[f]+")\n";
      }
      }
      stack1.push(f);
  }
  else if(value->name != NULL && (alloc_pos.find(value->name) != alloc_pos.end())){
      if(alloc_flag[value->name]==1){
      int f = 0;
      for(int i=0;i<15;i++){
          if(used[i] == 0){
              used[i] = 1;f=i;
              break;
          }
      }
      if(alloc_pos[value->name]<0){
        string s1=value->name;
        s1.erase(s1.begin());
        riscv+="la "+reg[f]+","+s1+"\n";
      }
      else{
      if(sp_max<=1024){
        riscv+="lw "+reg[f]+","+to_string(alloc_pos[value->name])+"(sp)\n";
      }
      else{
        riscv+="li "+reg[f]+","+to_string(alloc_pos[value->name])+"\n";
        riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
        riscv+="lw "+reg[f]+",0("+reg[f]+")\n";
      }
      }
      stack1.push(f);
      }
  }
  else{
    // 根据指令类型判断后续需要如何访问
    const auto &kind = value->kind;
    switch (kind.tag) {
      case KOOPA_RVT_RETURN:
        // 访问 return 指令
        
        Visit(kind.data.ret);
        break;
      case KOOPA_RVT_INTEGER:
        // 访问 integer 指令
        Visit(kind.data.integer);
        break;
      case KOOPA_RVT_BINARY:
        if(symbol1.find(value) == symbol1.end()){
          int size3=stack1.size();
          Visit(kind.data.binary);
          symbol1[value] = sp_max;
          int p=0;if(stack1.size()>size3)p=stack1.top(),stack1.pop();
          if(sp_max<=1024)
          riscv+="sw "+reg[p]+","+to_string(sp_max)+"(sp)\n";
          else{
            int f = 0;
            for(int i = 0 ; i <15 ; i++){
                if(used[i] == 0){
                    f = i;
                    break;
                }
            }
            riscv+="li "+reg[f]+","+to_string(sp_max)+"\n";
              riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
              riscv+="sw "+reg[p]+",0("+reg[f]+")\n";
          }
          used[p] = 0;
          
          sp_max += 4;
        }
        else{
            int f = 0;
            for(int i = 0 ; i <15 ; i++){
                if(used[i] == 0){
                    used[i] = 1;
                    f = i;
                    break;
                }
            }
            if(symbol1[value]<=1024)
            riscv+="lw "+reg[f]+","+to_string(symbol1[value])+"(sp)\n";
            else{
              riscv+="li "+reg[f]+","+to_string(symbol1[value])+"\n";
              riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
              riscv+="lw "+reg[f]+",0("+reg[f]+")\n";
            }
            stack1.push(f);
        }
        break;
       case KOOPA_RVT_STORE:{
          
          Visit(kind.data.store);
          break;
       }
      case KOOPA_RVT_ALLOC:{
        
          auto ii=value->ty->data.pointer.base;
          vector<int> s1;
          int ll=1;
          while(1){
            if(ii->tag==0)break;
            if(ii->tag==3){s1.push_back(1);
              if(ii->data.pointer.base->tag==2){
              ii=ii->data.pointer.base;
              //s1.push_back(ii->data.array.len);
              //ll*=ii->data.array.len;
            continue;
              }
              else if(ii->data.pointer.base->tag==3){
                
                ii=ii->data.pointer.base;continue;
              }
              else if(ii->data.pointer.base->tag==0)break;
            }
            s1.push_back(ii->data.array.len);
            ll*=ii->data.array.len;
            if(ii->data.array.base->tag==0)break;
            ii=ii->data.array.base;
          }
          alloc_size[value->name]=s1;
          //cout<<alloc_size[value->name][0]<<alloc_size[value->name][1]<<"size1\n";
          //cout<<s1.size()<<"alloc\n";
          if(value->name != NULL){
              if(alloc_pos.find(value->name) == alloc_pos.end()){
                  alloc_pos[value->name] = sp_max;
                  alloc_flag[value->name]=0;
                  sp_max += 4*ll;
              }
              
          }
          break;
      }
      case KOOPA_RVT_LOAD:{
          Visit(kind.data.load);
         
          //riscv+="sw "+reg[stack1.top()]+","+to_string(sp_max)+"(sp)\n";
          if(sp_max<=1024){
              riscv+="sw "+reg[stack1.top()]+","+to_string(sp_max)+"(sp)\n";
            }
            else{
              int f = 0;
              for(int i = 0 ; i <15; i++){ 
                  if(used[i] == 0){
                      f= i;break;
                  }
              }
              riscv+="li "+reg[f]+","+to_string(sp_max)+"\n";
              riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
              riscv+="sw "+reg[stack1.top()]+",0("+reg[f]+")\n";
            }
             used[stack1.top()] = 0;
          stack1.pop();
          symbol1[value] = sp_max;
          sp_max+=4;
          break;
      }
      case KOOPA_RVT_BRANCH:{
          Visit(kind.data.branch);break;
      }
      case KOOPA_RVT_JUMP:{
          Visit(kind.data.jump);break;
      }
      case KOOPA_RVT_GET_PTR:{
          if(symbol1.find(value) == symbol1.end()){
          int size3=stack1.size();
          Visit(kind.data.get_ptr);
          symbol1[value] = sp_max;
          int p=0;if(stack1.size()>size3)p=stack1.top(),stack1.pop();
          
          if(sp_max<=1024)
          riscv+="sw "+reg[p]+","+to_string(sp_max)+"(sp)\n";
          else{
            int f = 0;
            for(int i = 0 ; i <15 ; i++){
                if(used[i] == 0){
                    f = i;
                    break;
                }
            }
            riscv+="li "+reg[f]+","+to_string(sp_max)+"\n";
              riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
              riscv+="sw "+reg[p]+",0("+reg[f]+")\n";
          }
          used[p] = 0;
          sp_max += 4;
        }
        else{
            int f = 0;
            for(int i = 0 ; i <15 ; i++){
                if(used[i] == 0){
                    used[i] = 1;
                    f = i;
                    break;
                }
            }
            riscv+="lw "+reg[f]+","+to_string(symbol1[value])+"(sp)\n";
            stack1.push(f);
        }
          break;
      }
      case KOOPA_RVT_GET_ELEM_PTR:{
          
          if(symbol1.find(value) == symbol1.end()){
          int size3=stack1.size();
          Visit(kind.data.get_elem_ptr);
          symbol1[value] = sp_max;
          int p=0;if(stack1.size()>size3)p=stack1.top(),stack1.pop();
          
          if(sp_max<=1024)
          riscv+="sw "+reg[p]+","+to_string(sp_max)+"(sp)\n";
          else{
            int f = 0;
            for(int i = 0 ; i <15 ; i++){
                if(used[i] == 0){
                    f = i;
                    break;
                }
            }
            riscv+="li "+reg[f]+","+to_string(sp_max)+"\n";
              riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
              riscv+="sw "+reg[p]+",0("+reg[f]+")\n";
          }
          used[p] = 0;
          
          sp_max += 4;
        }
        else{
            int f = 0;
            for(int i = 0 ; i <15 ; i++){
                if(used[i] == 0){
                    used[i] = 1;
                    f = i;
                    break;
                }
            }
            riscv+="lw "+reg[f]+","+to_string(symbol1[value])+"(sp)\n";
            stack1.push(f);
        }
          break;
      }
      case KOOPA_RVT_CALL:{
          Visit(kind.data.call);
          
          if(sp_max<=1024)
          riscv+="sw "+reg[stack1.top()]+","+to_string(sp_max)+"(sp)\n";
          else{
            int f = 0;
            for(int i = 0 ; i <15 ; i++){
                if(used[i] == 0){
                    f = i;
                    break;
                }
            }
            riscv+="li "+reg[f]+","+to_string(sp_max)+"\n";
              riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
              riscv+="sw "+reg[stack1.top()]+",0("+reg[f]+")\n";
          }
          used[stack1.top()] = 0;
          stack1.pop();
          symbol1[value] = sp_max;
          sp_max+=4;
          break;

      }
      case KOOPA_RVT_GLOBAL_ALLOC:{
          auto ii=value->ty->data.pointer.base->data.array;
          vector<int> s11;
          while(1){
            if(value->ty->data.pointer.base->tag!=2)break;
            s11.push_back(ii.len);
            if(ii.base->tag==0)break;
            ii=ii.base->data.array;
          }
          alloc_size[value->name]=s11;
          riscv+="  .data\n  .globl ";
          string s1=value->name;
          s1.erase(s1.begin());
          riscv+=s1+"\n";
          riscv+=s1+":\n";
          
          Visit(kind.data.global_alloc);
          alloc_pos[value->name] = -1;
          alloc_flag[value->name]=1;
          
          break;
      }
      case  KOOPA_RVT_FUNC_ARG_REF  :{
          Visit(kind.data.func_arg_ref);
          break;
      }
      case KOOPA_RVT_ZERO_INIT:{
          riscv+=".zero "+to_string(zeroinit)+"\n";
          break;
      }
      case KOOPA_RVT_AGGREGATE:{
        Visit(kind.data.aggregate);break;
      }
      default:{
        // 其他类型暂时遇不到
        cout<<value->kind.tag<<"????\n";
        assert(false);
      } 
    }
  }
}


void Visit(const koopa_raw_integer_t &intval){
  if(global==0){
  for(int i = 0 ; i < 15 ; i ++){
    if(used[i] == 0){
      used[i] = 1;
      riscv+="li "+reg[i]+","+to_string(intval.value)+"\n";
      stack1.push(i);
      break;
    }
  }
  }
  else{
    stack1.push(intval.value);
  }
}

void Visit(const koopa_raw_return_t &ret){
  //use=1;
  if(ret.value){
    Visit(ret.value);
    int p1 = stack1.top();
    stack1.pop();
    riscv+="add a0,zero,"+reg[p1]+"\n";
    used[p1] = 0;
    
  }
  riscv+="li t0,40956\n";
  riscv+="add t0,sp,t0\n";
  riscv+="lw ra,0(t0)\n";
  riscv+="li t0,40960\n";
  riscv+="add sp,sp,t0\n";
  riscv+="ret\n";
  
}

void Visit(const koopa_raw_binary_t &bin){
  //use=1;
  Visit(bin.lhs);
  Visit(bin.rhs);
  Visit(bin.op);
  //use=0;
}

void Visit(const koopa_raw_binary_op_t &op){
  int p1 = stack1.top();
  stack1.pop();
  int p2 = stack1.top();
  switch(op){
    case KOOPA_RBO_NOT_EQ:{
      riscv+="xor "+reg[p2]+","+reg[p1]+","+reg[p2]+"\n";
      riscv+="snez "+reg[p2]+","+reg[p2]+"\n";
      used[p1] = 0;
      break;
    }
    case KOOPA_RBO_EQ:{
      riscv+="xor "+reg[p2]+","+reg[p1]+","+reg[p2]+"\n";
      riscv+="seqz "+reg[p2]+","+reg[p2]+"\n";
      used[p1] = 0;
      break;
    }
    case KOOPA_RBO_GT:{
      riscv+="sgt "+reg[p2]+","+reg[p2]+","+reg[p1]+"\n";
      used[p1] = 0;
      break;
    }
    case KOOPA_RBO_LT:{
      riscv+="slt "+reg[p2]+","+reg[p2]+","+reg[p1]+"\n";
      used[p1] = 0;
      break;
    }
    break;
    case KOOPA_RBO_GE:{
      riscv+="slt "+reg[p2]+","+reg[p2]+","+reg[p1]+"\n";
      riscv+="seqz "+reg[p2]+","+reg[p2]+"\n";
      used[p1] = 0;
      break;
    }
    case KOOPA_RBO_LE:{
      riscv+="sgt "+reg[p2]+","+reg[p2]+","+reg[p1]+"\n";
      riscv+="seqz "+reg[p2]+","+reg[p2]+"\n";
      used[p1] = 0;
      break;
    }
    case KOOPA_RBO_ADD:{
      riscv+="add "+reg[p2]+","+reg[p1]+","+reg[p2]+"\n";
      used[p1] = 0;
      break;
    }
    case KOOPA_RBO_SUB:{
      riscv+="sub "+reg[p2]+","+reg[p2]+","+reg[p1]+"\n";
      used[p1] = 0;
      break;
    }
    case KOOPA_RBO_MUL:{
      riscv+="mul "+reg[p2]+","+reg[p1]+","+reg[p2]+"\n";
      used[p1] = 0;
      break;
    }
    case KOOPA_RBO_DIV:{
      riscv+="div "+reg[p2]+","+reg[p2]+","+reg[p1]+"\n";
      used[p1] = 0;
      break;
    }
    case KOOPA_RBO_MOD:{
      int tmp1 = 0;
      for(int i = 0 ; i < 7 ; i ++){
        if(used[i] == 0){
          used[i] = 1;
          tmp1 = i;
          break;
        }
      }
      riscv+="div "+reg[tmp1]+","+reg[p2]+","+reg[p1]+"\n";
      riscv+="mul "+reg[p1]+","+reg[p1]+","+reg[tmp1]+"\n";
      riscv+="sub "+reg[p2]+","+reg[p2]+","+reg[p1]+"\n";
      used[p1] = 0;
      used[tmp1] = 0;
      break;
    }
    case KOOPA_RBO_AND:{
      riscv+="and "+reg[p2]+","+reg[p1]+","+reg[p2]+"\n";
      used[p1] = 0;
      break;
    }
    case KOOPA_RBO_OR:{
      riscv+="or "+reg[p2]+","+reg[p1]+","+reg[p2]+"\n";
      used[p1] = 0;
      break;
    }
    
  }
}
void Visit(const koopa_raw_store_t &store){

    Visit(store.value);
    /*if(store.value->name){
      if(alloc_pos.find(store.value->name)==alloc_pos.end()){
        
        alloc_pos[store.value->name]=sp_max,sp_max+=4;
      }
    }*/
    int loadreg = stack1.top();
    stack1.pop();
    if(store.dest->name){
    if(alloc_pos[store.dest->name]<0){
      string s1=store.dest->name;
      s1.erase(s1.begin());
      int f = 0;
        for(int i = 0 ; i <15; i++){ 
            if(used[i] == 0){
                f= i;break;
            }
        }
      riscv+="la "+reg[f]+","+s1+"\n";
      riscv+="sw "+reg[loadreg]+",0("+reg[f]+")\n";
      used[f]=0;
    }
    else{
    
      if(alloc_pos[store.dest->name]<=1024){
        riscv+="sw "+reg[loadreg]+","+to_string(alloc_pos[store.dest->name])+"(sp)\n";
      }
      else{
        int f = 0;
        for(int i = 0 ; i <15; i++){ 
            if(used[i] == 0){
                f= i;break;
            }
        }
        riscv+="li "+reg[f]+","+to_string(alloc_pos[store.dest->name])+"\n";
        riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
        riscv+="sw "+reg[loadreg]+",0("+reg[f]+")\n";
      }
    }
    alloc_flag[store.dest->name]=1;
    }
    else{
      if(!stack1.empty()){
      int p1=stack1.top();
      stack1.pop();
      riscv+="sw "+reg[loadreg]+",0("+reg[p1]+")\n";used[p1]=0;
      }
      else{
        int f = 0;
        for(int i = 0 ; i <15; i++){ 
            if(used[i] == 0){
                f= i;break;
            }
        }
        //riscv+="lw "+reg[f]+","+to_string(symbol1[store.dest])+"(sp)\n";
        riscv+="li "+reg[f]+","+to_string(symbol1[store.dest])+"\n";
        riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
        riscv+="lw "+reg[f]+",0"+"("+reg[f]+")\n";
        riscv+="sw "+reg[loadreg]+",0"+"("+reg[f]+")\n";
      }
    }
    used[loadreg] = 0;//sp_max+=4;
    
    
    
}

void Visit(const koopa_raw_load_t &load){
    int f = 0;
        for(int i = 0 ; i <15; i++){ 
            if(used[i] == 0){
                used[i] = 1;
                f= i;break;
            }
        }
    if(load.src->name){
    if(alloc_pos[load.src->name]<0){
      string s1=load.src->name;
      s1.erase(s1.begin());
      riscv+="la "+reg[f]+","+s1+"\n";
      riscv+="lw "+reg[f]+",0("+reg[f]+")\n";
    }
    else{
      if(sp_max<=1024){
        riscv+="lw "+reg[f]+","+to_string(alloc_pos[load.src->name])+"(sp)\n";
      }
      else{
        riscv+="li "+reg[f]+","+to_string(alloc_pos[load.src->name])+"\n";
        riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
        riscv+="lw "+reg[f]+",0("+reg[f]+")\n";
      }
    }
    }
    else{
      if(!stack1.empty()){
      int p1=stack1.top();stack1.pop();
      riscv+="lw "+reg[f]+",0("+reg[p1]+")\n";
      used[p1]=0;
      }
      else {
        //riscv+="lw "+reg[f]+","+to_string(symbol1[load.src])+"(sp)\n";
        riscv+="li "+reg[f]+","+to_string(symbol1[load.src])+"\n";
        riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
        riscv+="lw "+reg[f]+",0"+"("+reg[f]+")\n";
        riscv+="lw "+reg[f]+",0"+"("+reg[f]+")\n";
      }
    }
    stack1.push(f);
    
}

void Visit(const koopa_raw_branch_t &branch){
  
  Visit(branch.cond);int freg=stack1.top();stack1.pop();
  string s1=branch.true_bb->name;
  s1.erase(s1.begin());
  string s2=branch.false_bb->name;
  s2.erase(s2.begin());
  riscv+="bnez "+reg[freg]+","+s1+"\n";
  riscv+="j "+s2+"\n";
  used[freg]=0;
  //Visit(branch.true_bb);Visit(branch.false_bb);
}
void Visit(const koopa_raw_jump_t &jump){

string s1=jump.target->name;
s1.erase(s1.begin());
riscv+="j "+s1+"\n";
//Visit(jump.target);

}
void Visit(const koopa_raw_global_alloc_t&global_alloc){
  global=1;
  int size1=stack1.size();
  if(global_alloc.init->ty->tag==KOOPA_RTT_ARRAY){
            zeroinit=4*global_alloc.init->ty->data.array.len;
  }
  else zeroinit=4;
  Visit(global_alloc.init);
  global=0;
  vector<int> tmp1;
  if(stack1.size()-size1>0){
    while(stack1.size()>size1)
    tmp1.push_back(stack1.top()),stack1.pop();
    for(int i=tmp1.size() -1; i >= 0; --i)
    riscv+=".word "+to_string(tmp1[i])+"\n";
  }
}
void Visit(const koopa_raw_call_t &call){
  change.clear();change1.clear();
  
  callp=1;
  Visit(call.args);
  callp=0;
  int sp1=sp_max;
  if(sp1<=1024)
  riscv+="sw ra,"+to_string(sp_max)+"(sp)\n";
  else
  {
    int f = 0;
        for(int j = 0 ; j <15; j++){ 
            if(used[j] == 0){
                f= j;break;
            }
        }
    riscv+="li "+reg[f]+","+to_string(sp1)+"\n";
    riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
    riscv+="sw ra,0("+reg[f]+")\n";
  }
 
  
  string sn=call.callee->name;sn.erase(sn.begin());
  riscv+="call "+sn+"\n";
  sp_max=sp1;
  if(sp1<=1024)
  riscv+="lw ra,"+to_string(sp1)+"(sp)\n";
  else
  {
    int f = 0;
        for(int j = 0 ; j <15; j++){ 
            if(used[j] == 0){
                f= j;break;
            }
        }
    riscv+="li "+reg[f]+","+to_string(sp1)+"\n";
    riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
    riscv+="lw ra,0("+reg[f]+")\n";
  }
  /*int tmp1=0;
  if(!change1.empty())
  tmp1=change1.size()-1;
  for(int i=change.size()-1;i>=0;i--){
    int f = 0;
        for(int j = 0 ; j <15; j++){ 
            if(used[j] == 0){
                f= j;break;
            }
        }
        if(change[i]<=1024)
    riscv+="lw "+reg[f]+","+to_string(change[i])+"(sp)\n";
    else{
      riscv+="li "+reg[f]+","+to_string(change[i])+"\n";
        riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
        riscv+="lw "+reg[f]+",0("+reg[f]+")\n";
    }
    if(change1[tmp1]>=-1024)
      riscv+="sw "+reg[f]+","+to_string(change1[tmp1])+"(sp)\n";
      else{
        int f1 = 0;
        for(int j = 0 ; j <15; j++){ 
            if(used[j] == 0&&j!=f){
                f1= j;break;
            }
        }
        riscv+="li "+reg[f1]+","+to_string(change1[tmp1])+"\n";
        riscv+="add "+reg[f1]+",sp,"+reg[f1]+"\n";
        riscv+="sw "+reg[f]+",0("+reg[f1]+")\n";
      }
      tmp1-=1;
  }*/
  int f = 0;
  for(int j = 0 ; j <15; j++){ 
            if(used[j] == 0){
              used[j]=1;
                f= j;break;
            }
  }
  riscv+="add "+reg[f]+",a0,zero\n";
  stack1.push(f);
}
void Visit(const koopa_raw_func_arg_ref_t&func_arg_ref){
  //int nums = func_arg_ref.index;
  
}
void Visit(const koopa_raw_get_ptr_t&g){
  int f = 0;
  for(int j = 0 ; j <15; j++){ 
            if(used[j] == 0){
              used[j]=1;
                f= j;break;
            }
  }
  if(g.src->name){
  string s1=g.src->name;
  s1.erase(s1.begin());
  if(alloc_pos[g.src->name]>=0){
  riscv+="addi "+reg[f]+",sp,"+to_string(alloc_pos[g.src->name])+"\n";
  }
  else{
    riscv+="la "+reg[f]+","+string(s1)+"\n";
  }
  sizea.clear();
  for(int i=0;i<alloc_size[g.src->name].size();i++){
    sizea.push_back(alloc_size[g.src->name][i]);
  }
  }
  else{
    //int p1=stack1.top();
    string name1=g.src->kind.data.load.src->name;
    sizea.clear();
  for(int i=0;i<alloc_size[name1].size();i++){
    sizea.push_back(alloc_size[name1][i]);
  }
  riscv+="li "+reg[f]+","+to_string(symbol1[g.src])+"\n";
        riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
        riscv+="lw "+reg[f]+",0("+reg[f]+")\n";
    //riscv+="lw "+reg[f]+","+to_string(symbol1[g.src])+"(sp)\n";
    //riscv+="add "+reg[f]+",zero,"+reg[f]+"\n";
    //used[p1]=0;
  }
  Visit(g.index);
  int offset=1;
  auto ii=g.src;
  int size2=0;
  while(ii->kind.tag==11){
    size2++;
    ii=ii->kind.data.get_elem_ptr.src;
  }
  //cout<<size2<<" "<<stack1.size()<<"size4"<<endl;
  //if(size2+1!=stack1.size())used[stack1.top()]=0,stack1.pop();
  cout<<size2<<" "<<sizea.size()<<"size2\n";
  for(int j=sizea.size()-1;j>size2;j--){
    offset*=sizea[j];
  }
  int f2 = 0;
  for(int j = 0 ; j <15; j++){ 
            if(used[j] == 0){
                f2= j;break;
            }
  }
  
  int p1=stack1.top();
  stack1.pop();
  riscv+="li "+reg[f2]+","+to_string(4*offset)+"\n";
  riscv+="mul "+reg[p1]+","+reg[p1]+","+reg[f2]+"\n";
  riscv+="add "+reg[f]+","+reg[f]+","+reg[p1]+"\n";
  stack1.push(f);used[p1]=0;
}
void Visit(const koopa_raw_get_elem_ptr_t &g){
    int f = 0;
  for(int j = 0 ; j <15; j++){ 
            if(used[j] == 0){
              used[j]=1;
                f= j;break;
            }
  }
  if(g.src->name){
  string s1=g.src->name;
  s1.erase(s1.begin());
  if(alloc_pos[g.src->name]>=0){
  riscv+="addi "+reg[f]+",sp,"+to_string(alloc_pos[g.src->name])+"\n";
  }
  else{
    riscv+="la "+reg[f]+","+string(s1)+"\n";
  }
  sizea.clear();
  for(int i=0;i<alloc_size[g.src->name].size();i++){
    sizea.push_back(alloc_size[g.src->name][i]);
  }
  }
  else{
      //cout<<riscv<<endl;
      //cout<<riscv<<endl;
    //int p1=stack1.top();
    //riscv+="add "+reg[f]+",zero,"+reg[p1]+"\n";
    if(symbol1[g.src]<=1024)
    riscv+="lw "+reg[f]+","+to_string(symbol1[g.src])+"(sp)\n";
    else{
      riscv+="li "+reg[f]+","+to_string(symbol1[g.src])+"\n";
        riscv+="add "+reg[f]+",sp,"+reg[f]+"\n";
        riscv+="lw "+reg[f]+",0("+reg[f]+")\n";
    }
    //used[p1]=0;
  }
  Visit(g.index);
  int offset=1;
  auto ii=g.src;
  int size2=0;
  while(ii->kind.tag==11||ii->kind.tag==10){
    size2++;
    ii=ii->kind.data.get_elem_ptr.src;
  }
  //cout<<size2<<" "<<stack1.size()<<"size4"<<endl;
  //if(size2+1!=stack1.size())used[stack1.top()]=0,stack1.pop();
  for(int j=sizea.size()-1;j>size2;j--){
    offset*=sizea[j];
  }
  int f2 = 0;
  for(int j = 0 ; j <15; j++){ 
            if(used[j] == 0){
                f2= j;break;
            }
  }
  
  int p1=stack1.top();
  stack1.pop();
  riscv+="li "+reg[f2]+","+to_string(4*offset)+"\n";
  riscv+="mul "+reg[p1]+","+reg[p1]+","+reg[f2]+"\n";
  riscv+="add "+reg[f]+","+reg[f]+","+reg[p1]+"\n";
  stack1.push(f);used[p1]=0;
}
void Visit(const koopa_raw_aggregate_t&a){
  cout<<a.elems.len<<" "<<a.elems.kind<<"len\n";
  //int size3=stack1.size();
  Visit(a.elems);
  //while(stack1.size()>)
  /*for(int i=0;i<a.elems.len;i++){
    auto ptr = a.elems.buffer[i];
    int s1=reinterpret_cast<koopa_raw_value_t>(ptr)->kind.data.integer.value;
    riscv+=".word "+to_string(s1)+"\n";
  }*/
}