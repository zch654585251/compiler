decl @getint(): i32
decl @getch(): i32
decl @getarray(*i32): i32
decl @putint(i32)
decl @putch(i32)
decl @putarray(i32, *i32)
decl @starttime()
decl @stoptime()
global @n_0 = alloc i32,zeroinit
fun @heap_ajust(@arr:*i32,@start:i32): i32 {
%entry1:
@arr_1 = alloc *i32
store @arr,@arr_1
@start_1 = alloc i32
store @start,@start_1
%0 = load @arr_1
%1= getptr %0,1
%2 = load %1
ret %2
}
fun @heap_sort(@arr:*i32,@len:i32): i32 {
%entry2:
@arr_1 = alloc *i32
store @arr,@arr_1
@len_1 = alloc i32
store @len,@len_1
%3= load @arr_1
%4= getptr %3,0
%5=call @heap_ajust(%4,0)
ret %5
}
fun @main(): i32 {
%entry3:
store 10,@n_0
@a_1=alloc[i32,10]
%6= getelemptr @a_1,0
%7 = load @n_0
%8=call @heap_sort(%6,%7)
ret 0
}
