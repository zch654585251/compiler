#include <cassert>
#include <cstdio>
#include <iostream>
#include <memory>
#include <string>
#include "ast.h"
#include "koopa.h"
#include "k2r.h"
using namespace std;

// ���� lexer ������, �Լ� parser ����
// Ϊʲô������ sysy.tab.hpp ��? ��Ϊ��������û�� yyin �Ķ���
// ���, ��Ϊ����ļ����������Լ�д��, ���Ǳ� Bison ���ɳ�����
// ��Ĵ���༭��/IDE �ܿ����Ҳ�������ļ�, Ȼ�����㱨�� (��Ȼ���벻�����)
// ��������ܷ���, ���Ǹɴ�������ֿ����� dirty ��ʵ�ʺ���Ч���ֶ�
extern FILE *yyin;
extern FILE *yyout;
extern char* yytext;
string BaseAST::out="";
//string riscv="";
extern int yyparse(std::unique_ptr<BaseAST> &ast);

int main(int argc, const char *argv[]) {
  // ���������в���. ���Խű�/����ƽ̨Ҫ����ı������ܽ������²���:
  // compiler ģʽ �����ļ� -o ����ļ�
  assert(argc == 5);
  auto mode = argv[1];
  auto input = argv[2];
  auto output = argv[4];

  // �������ļ�, ����ָ�� lexer �ڽ�����ʱ���ȡ����ļ�
  yyin = fopen(input, "r");
  assert(yyin);

  unique_ptr<BaseAST> ast;
auto ret = yyparse(ast);


// dump AST
ast->Dump();

//yytext=(char*)ast->out.data();
yyout=fopen(output,"w");
if(mode==string("-koopa")){
cout<<ast->out;
cout << endl;
fprintf(yyout,"%s",(char*)ast->out.data());
assert(yyout);
}

if(mode==string("-riscv")){

// 解析字符串 str, 得到 Koopa IR 程序
koopa_program_t program;
koopa_error_code_t ret1 = koopa_parse_from_string((char*)ast->out.data(), &program);
assert(ret1 == KOOPA_EC_SUCCESS);  // 确保解析时没有出错
// 创建一个 raw program builder, 用来构建 raw program
koopa_raw_program_builder_t builder = koopa_new_raw_program_builder();
// 将 Koopa IR 程序转换为 raw program
koopa_raw_program_t raw = koopa_build_raw_program(builder, program);
// 释放 Koopa IR 程序占用的内存
koopa_delete_program(program);
Visit(raw);
// 处理 raw program
// ...
// 使用 for 循环遍历函数列表

// 处理完成, 释放 raw program builder 占用的内存
// 注意, raw program 中所有的指针指向的内存均为 raw program builder 的内存
// 所以不要在 raw program builder 处理完毕之前释放 builder
koopa_delete_raw_program_builder(builder);
cout<<riscv<<endl;
fprintf(yyout,"%s",(char*)riscv.data());
assert(yyout);
}
assert(!ret);
  return 0;
}
