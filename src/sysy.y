%code requires {
  #include <memory>
  #include <string>
}

%{

#include <iostream>
#include <memory>
#include <string>
#include "ast.h"

// 声明 lexer 函数和错误处理函数
int yylex();
void yyerror(std::unique_ptr<BaseAST> &ast, const char *s);

using namespace std;

%}

// 定义 parser 函数和错误处理函数的附加参数
// 我们需要返回一个字符串作为 AST, 所以我们把附加参数定义成字符串的智能指针
// 解析完成后, 我们要手动修改这个参数, 把它设置成解析得到的字符串
//%parse-param { std::unique_ptr<std::string> &ast }
%parse-param { std::unique_ptr<BaseAST> &ast }

// yylval 的定义, 我们把它定义成了一个联合体 (union)
// 因为 token 的值有的是字符串指针, 有的是整数
// 之前我们在 lexer 中用到的 str_val 和 int_val 就是在这里被定义的
// 至于为什么要用字符串指针而不直接用 string 或者 unique_ptr<string>?
// 请自行 STFW 在 union 里写一个带析构函数的类会出现什么情况
%union {
  std::string *str_val;
  int int_val;
  BaseAST *ast_val;
}

// lexer 返回的所有 token 种类的声明
// 注意 IDENT 和 INT_CONST 会返回 token 的值, 分别对应 str_val 和 int_val
%token INT RETURN CONST IF ELSE WHILE BREAK CONTINUE VOID
%token <str_val> IDENT DOp1 DOp2 DOp3 DOp4
%token <int_val> INT_CONST

// 非终结符的类型定义
%type <ast_val> CompUnit FuncDef Block Stmt Exp PrimaryExp UnaryExp UnaryOp MulExp AddExp LOrExp LAndExp EqExp RelExp Decl ConstDecl ConstDef ConstInitVal BlockItem ConstExp ConstDefs VarDecl VarDefs ValDef FuncFParams FuncFParam FuncRParams ConstExp1 ConstExp2 Exps Expss InitVal
%type <str_val> LVal
%type <int_val> Number

%%

// 开始符, CompUnit ::= FuncDef, 大括号后声明了解析完成后 parser 要做的事情
// 之前我们定义了 FuncDef 会返回一个 str_val, 也就是字符串指针
// 而 parser 一旦解析完 CompUnit, 就说明所有的 token 都被解析了, 即解析结束了
// 此时我们应该把 FuncDef 返回的结果收集起来, 作为 AST 传给调用 parser 的函数
// $1 指代规则里第一个符号的返回值, 也就是 FuncDef 的返回值
CompUnits
: CompUnit{
  auto comp_unit = make_unique<CompUnitsAST>();
    comp_unit->comp1 = unique_ptr<BaseAST>($1);
    comp_unit->mode=0;
    ast = move(comp_unit);
}
;

CompUnit
  : FuncDef {
    auto comp_unit = new CompUnitAST();
    comp_unit->func_def = unique_ptr<BaseAST>($1);
    comp_unit->mode=0;
    $$=comp_unit;
  }


  |FuncDef CompUnit {
    auto comp_unit = new CompUnitAST();
    comp_unit->comp=unique_ptr<BaseAST>($1);
    comp_unit->func_def = unique_ptr<BaseAST>($2);
    comp_unit->mode=1;
    $$=comp_unit;
  }
  |Decl {
    auto comp_unit = new CompUnitAST();
    comp_unit->func_def = unique_ptr<BaseAST>($1);
    comp_unit->mode=0;
    $$=comp_unit;
  }


  |Decl CompUnit {
    auto comp_unit = new CompUnitAST();
    comp_unit->comp=unique_ptr<BaseAST>($1);
    comp_unit->func_def = unique_ptr<BaseAST>($2);
    comp_unit->mode=1;
    $$=comp_unit;
  }
  ;

// FuncDef ::= FuncType IDENT '(' ')' Block;
// 我们这里可以直接写 '(' 和 ')', 因为之前在 lexer 里已经处理了单个字符的情况
// 解析完成后, 把这些符号的结果收集起来, 然后拼成一个新的字符串, 作为结果返回
// $$ 表示非终结符的返回值, 我们可以通过给这个符号赋值的方法来返回结果
// 你可能会问, FuncType, IDENT 之类的结果已经是字符串指针了
// 为什么还要用 unique_ptr 接住它们, 然后再解引用, 把它们拼成另一个字符串指针呢
// 因为所有的字符串指针都是我们 new 出来的, new 出来的内存一定要 delete
// 否则会发生内存泄漏, 而 unique_ptr 这种智能指针可以自动帮我们 delete
// 虽然此处你看不出用 unique_ptr 和手动 delete 的区别, 但当我们定义了 AST 之后
// 这种写法会省下很多内存管理的负担
FuncDef
  : INT IDENT '(' ')' Block {
    auto ast = new FuncDefAST();
    ast->s=*unique_ptr<string>(new string(": i32"));
    ast->ident = *unique_ptr<string>($2);
    ast->block = unique_ptr<BaseAST>($5);
    ast->mode=0;
    $$ = ast;
  }
  | INT IDENT '(' FuncFParams ')' Block {
    auto ast = new FuncDefAST();
    ast->s=*unique_ptr<string>(new string(": i32"));
    ast->ident = *unique_ptr<string>($2);
    ast->block = unique_ptr<BaseAST>($6);
    ast->func=unique_ptr<BaseAST>($4);
    ast->mode=1;
    $$ = ast;
  }
  | VOID IDENT '(' ')' Block {
    auto ast = new FuncDefAST();
    ast->s=*unique_ptr<string>(new string(""));
    ast->ident = *unique_ptr<string>($2);
    ast->block = unique_ptr<BaseAST>($5);
    ast->mode=0;
    $$ = ast;
  }
  | VOID IDENT '(' FuncFParams ')' Block {
    auto ast = new FuncDefAST();
    ast->s=*unique_ptr<string>(new string(""));
    ast->ident = *unique_ptr<string>($2);
    ast->block = unique_ptr<BaseAST>($6);
    ast->func=unique_ptr<BaseAST>($4);
    ast->mode=1;
    $$ = ast;
  }
  ;

// 同上, 不再解释

FuncFParams
:FuncFParam{
  auto ast = new FuncFParamsAST();
  ast->constdef=unique_ptr<BaseAST>($1);
  ast->mode=0;
  $$=ast;
}
|FuncFParams ',' FuncFParam{
  auto ast = new FuncFParamsAST();
  ast->constdefs=unique_ptr<BaseAST>($1);
  ast->constdef=unique_ptr<BaseAST>($3);
  ast->mode=1;
  $$=ast;
}
;

FuncFParam
: INT IDENT{
    auto ast = new FuncFParamAST();
    ast->s=*unique_ptr<string>($2);
    ast->mode=0;
    $$=ast; 
}
| INT IDENT '[' ']' ConstExp1{
    auto ast = new FuncFParamAST();
    ast->s=*unique_ptr<string>($2);
    ast->exp=unique_ptr<BaseAST>($5);
    ast->mode=1;
    $$=ast; 
}
| INT IDENT '[' ']'{
    auto ast = new FuncFParamAST();
    ast->s=*unique_ptr<string>($2);
    ast->mode=2;
    $$=ast; 
}
;


Block
  : '{' BlockItem '}' {
    auto ast = new BlockAST();
    ast->blockitems=unique_ptr<BaseAST>($2);ast->mode=0;
    $$=ast;
  }
  |'{''}'{auto ast = new BlockAST();ast->mode=1;$$=ast;}
  ;
BlockItem
: Stmt{
  auto ast = new BlockItemAST();
  ast->mode=1;
  ast->stmt=unique_ptr<BaseAST>($1);
  $$=ast;
}
| Decl{
  auto ast = new BlockItemAST();
  ast->mode=0;
  ast->decl=unique_ptr<BaseAST>($1);
  $$=ast;
}
|BlockItem Stmt{
    auto ast = new BlockItemAST();
    ast->mode=3;
    ast->stmt=unique_ptr<BaseAST>($2);
    ast->blockitem=unique_ptr<BaseAST>($1);
    $$=ast;
}
| BlockItem Decl{
    auto ast = new BlockItemAST();
    ast->mode=2;
    ast->decl=unique_ptr<BaseAST>($2);
    ast->blockitem=unique_ptr<BaseAST>($1);
    $$=ast;
}
;
Stmt
  : RETURN Exp ';' {
    //auto number = new int($2);
    //$$ = new string("return " + *number + ";");
    auto ast = new StmtAST();
    ast->exp=unique_ptr<BaseAST>($2);
    //ast->s2="nmsl";
    ast->mode=0;
    $$=ast;
  }
  |LVal '=' Exp ';'{
    auto ast = new StmtAST();
    ast->s=*unique_ptr<string>($1);
    ast->exp=unique_ptr<BaseAST>($3);
    //ast->s2="nmsl";
    ast->mode=1;
    $$=ast;
  }
  | Exp ';'{
    auto ast = new StmtAST();
    ast->exp=unique_ptr<BaseAST>($1);
    ast->mode=2;
    $$=ast;
  }
  | Block{
    auto ast = new StmtAST();
    ast->block=unique_ptr<BaseAST>($1);
    ast->mode=3;
    $$=ast;
  }
  | ';'{
    auto ast = new StmtAST();
    ast->mode=4;
    $$=ast;
  }
  | RETURN ';'{
    auto ast = new StmtAST();
    ast->mode=11;
    $$=ast;
  }
  | IF '(' Exp ')' Stmt{
    auto ast = new StmtAST();
    ast->exp=unique_ptr<BaseAST>($3);
    ast->stmt1=unique_ptr<BaseAST>($5);
    ast->mode=5;
    $$=ast;
  }
  | IF '(' Exp ')' Stmt ELSE Stmt{
    auto ast = new StmtAST();
    ast->exp=unique_ptr<BaseAST>($3);
    ast->stmt1=unique_ptr<BaseAST>($5);
    ast->stmt2=unique_ptr<BaseAST>($7);
    ast->mode=6;
    $$=ast;
  }
  | WHILE '(' Exp ')' Stmt{
    auto ast = new StmtAST();
    ast->exp=unique_ptr<BaseAST>($3);
    ast->stmt1=unique_ptr<BaseAST>($5);
    ast->mode=7;
    $$=ast;
  }
  | BREAK ';'{
    auto ast = new StmtAST();
    ast->mode=8;
    $$=ast;
  }
  | CONTINUE ';'{
    auto ast = new StmtAST();
    ast->mode=9;
    $$=ast;
  }
  |IDENT Expss '=' Exp ';' {
    auto ast = new StmtAST();
    ast->mode=10;
    ast->s=*unique_ptr<string>($1);
    ast->exp=unique_ptr<BaseAST>($2);
    ast->stmt1=unique_ptr<BaseAST>($4);
    $$=ast;
  }
  ;
Expss
  : Expss '[' Exp ']'{
    auto ast = new ExpssAST();
    ast->mode=0;
    ast->exp1=unique_ptr<BaseAST>($1);
    ast->exp2=unique_ptr<BaseAST>($3);
    $$=ast;
  }
  | '[' Exp ']'{
    auto ast = new ExpssAST();
    ast->mode=1;
    ast->exp1=unique_ptr<BaseAST>($2);
    $$=ast;
  }
  ;
Exp
  : LOrExp{
    auto ast = new ExpAST();
    ast->unaryexp=unique_ptr<BaseAST>($1);
    $$=ast;
  }
  ;
LVal:
IDENT{
  $$=$1;
}
;
PrimaryExp
  : '(' Exp ')' {
    auto ast = new PrimaryExpAST();
    ast->mode=0;ast->exp=unique_ptr<BaseAST>($2);
    $$=ast;
  }
  | Number{
    auto ast = new PrimaryExpAST();
    ast->mode=1;ast->n=($1);
    $$=ast;
  }
  |LVal{
    auto ast = new PrimaryExpAST();
    ast->mode=2;ast->s=*unique_ptr<string>($1);
    $$=ast;
  }
  |LVal ConstExp1{
    auto ast = new PrimaryExpAST();
    ast->mode=3;ast->s=*unique_ptr<string>($1);ast->exp=unique_ptr<BaseAST>($2);
    $$=ast;
  }
  ;
UnaryExp
  : PrimaryExp {
    auto ast = new UnaryExpAST();
    ast->mode=0;ast->exp1=unique_ptr<BaseAST>($1);
    $$=ast;
  }
  |UnaryOp UnaryExp{
    auto ast = new UnaryExpAST();
    ast->mode=1;
    ast->exp1=unique_ptr<BaseAST>($1);
    ast->exp2=unique_ptr<BaseAST>($2);
    $$=ast;
  }
  | IDENT '(' FuncRParams ')'{
    auto ast = new UnaryExpAST();ast->s=*unique_ptr<string>($1);
    ast->mode=2;ast->funcr=unique_ptr<BaseAST>($3);
    $$=ast;
  }
  |IDENT '(' ')'{
    auto ast = new UnaryExpAST();ast->s=*unique_ptr<string>($1);
    ast->mode=3;
    $$=ast;
  }
  ;

FuncRParams
:Exp{
  auto ast = new FuncRParamsAST();
  ast->constdef=unique_ptr<BaseAST>($1);
  ast->mode=0;
  $$=ast;
}
|FuncRParams ',' Exp{
  auto ast = new FuncRParamsAST();
  ast->constdefs=unique_ptr<BaseAST>($1);
  ast->constdef=unique_ptr<BaseAST>($3);
  ast->mode=1;
  $$=ast;
}
;

UnaryOp
  : '+'{
    auto ast = new UnaryOpAST();
    ast->op="+";
    $$=ast;
  }
  | '-'{
    auto ast = new UnaryOpAST();
    ast->op="-";
    $$=ast;
  }
  | '!'{
    auto ast = new UnaryOpAST();
    ast->op="!";
    $$=ast;
  }
  ;
MulExp
:UnaryExp{
  auto ast = new MulExpAST();
  ast->mode=0;
  ast->unaryexp=unique_ptr<BaseAST>($1);
  $$=ast;
}
|MulExp '*' UnaryExp{
  auto ast = new MulExpAST();
  ast->mulexp=unique_ptr<BaseAST>($1);
  ast->op="*";
  ast->mode=1;
  ast->unaryexp=unique_ptr<BaseAST>($3);
  $$=ast;
}
|MulExp '/' UnaryExp{
  auto ast = new MulExpAST();
  ast->mulexp=unique_ptr<BaseAST>($1);
  ast->op="/";
  ast->mode=1;
  ast->unaryexp=unique_ptr<BaseAST>($3);
  $$=ast;
}
|MulExp '%' UnaryExp{
  auto ast = new MulExpAST();
  ast->mulexp=unique_ptr<BaseAST>($1);
  ast->op="%";
  ast->mode=1;
  ast->unaryexp=unique_ptr<BaseAST>($3);
  $$=ast;
}
;
AddExp
: MulExp{
  auto ast = new AddExpAST();
  ast->mulexp=unique_ptr<BaseAST>($1);
  ast->mode=0;
  $$=ast;
}
| AddExp '+' MulExp{
  auto ast = new AddExpAST();
  ast->addexp=unique_ptr<BaseAST>($1);
  ast->op="+";
  ast->mode=1;
  ast->mulexp=unique_ptr<BaseAST>($3);
  $$=ast;
}
| AddExp '-' MulExp{
  auto ast = new AddExpAST();
  ast->addexp=unique_ptr<BaseAST>($1);
  ast->op="-";
  ast->mode=1;
  ast->mulexp=unique_ptr<BaseAST>($3);
  $$=ast;
}
;
LOrExp
  :LAndExp{
  auto ast = new LOrExpAST();
  ast->landexp=unique_ptr<BaseAST>($1);
  ast->mode=0;
  $$=ast;
}
|LOrExp DOp1 LAndExp{
  auto ast = new LOrExpAST();
  ast->landexp=unique_ptr<BaseAST>($3);
  ast->op=*unique_ptr<string>($2);
  ast->mode=1;
  ast->lorexp=unique_ptr<BaseAST>($1);
  $$=ast;
}
LAndExp
  : EqExp{
    auto ast = new LAndExpAST();
    ast->eqexp=unique_ptr<BaseAST>($1);
    ast->mode=0;
    $$=ast;
  }
  |LAndExp DOp2 EqExp{
  auto ast = new LAndExpAST();
  ast->eqexp=unique_ptr<BaseAST>($3);
  ast->op=*unique_ptr<string>($2);
  ast->mode=1;
  ast->landexp=unique_ptr<BaseAST>($1);
  $$=ast;
}
  ;
EqExp
  : RelExp{
    auto ast = new EqExpAST();
    ast->relexp=unique_ptr<BaseAST>($1);
    ast->mode=0;
    $$=ast;
  }
  | EqExp DOp3 RelExp{
    auto ast = new EqExpAST();
    ast->relexp=unique_ptr<BaseAST>($3);
    ast->op=*unique_ptr<string>($2);
    ast->mode=1;
    ast->eqexp=unique_ptr<BaseAST>($1);
    $$=ast;
  }
  ;
RelExp
  : AddExp{
    auto ast = new RelExpAST();
    ast->addexp=unique_ptr<BaseAST>($1);
    ast->mode=0;
    $$=ast;
  }
  | RelExp '<' AddExp{
    auto ast = new RelExpAST();
    ast->relexp=unique_ptr<BaseAST>($1);
    ast->op="<";
    ast->mode=1;
    ast->addexp=unique_ptr<BaseAST>($3);
    $$=ast;
  }
  | RelExp '>' AddExp{
    auto ast = new RelExpAST();
    ast->relexp=unique_ptr<BaseAST>($1);
    ast->op=">";
    ast->mode=1;
    ast->addexp=unique_ptr<BaseAST>($3);
    $$=ast;
  }
  | RelExp DOp4 AddExp{
    auto ast = new RelExpAST();
    ast->relexp=unique_ptr<BaseAST>($1);
    ast->op=*unique_ptr<string>($2);;
    ast->mode=1;
    ast->addexp=unique_ptr<BaseAST>($3);
    $$=ast;
  }
  ;
Number
  : INT_CONST {
    //$$ = new string(to_string($1));
    //auto ast = new NumberAST();
    //ast->n=*unique_ptr<int>($1);
    $$=$1;
  }
  ;
Decl
: ConstDecl{
  auto ast = new DeclAST();
  ast->constdecl=unique_ptr<BaseAST>($1);
  ast->type=0;
  $$=ast;
}
| VarDecl{
  auto ast = new DeclAST();
  ast->constdecl=unique_ptr<BaseAST>($1);
  ast->type=1;
  $$=ast;
}
;
VarDecl
:INT VarDefs ';'{
  auto ast = new VarDeclAST();
  ast->constdefs=unique_ptr<BaseAST>($2);
  $$=ast;
}
;
VarDefs
:ValDef{
  auto ast = new VarDefsAST();
  ast->constdef=unique_ptr<BaseAST>($1);
  ast->mode=0;
  $$=ast;
}
|VarDefs ',' ValDef{
  auto ast = new VarDefsAST();
  ast->constdefs=unique_ptr<BaseAST>($1);
  ast->constdef=unique_ptr<BaseAST>($3);
  ast->mode=1;
  $$=ast;
}
;
ConstDecl
:CONST INT ConstDefs ';'{
  auto ast = new ConstDeclAST();
  ast->constdefs=unique_ptr<BaseAST>($3);
  $$=ast;
}
;
ConstDefs
:ConstDef{
  auto ast = new ConstDefsAST();
  ast->constdef=unique_ptr<BaseAST>($1);
  ast->mode=0;
  $$=ast;
}
|ConstDefs ',' ConstDef{
  auto ast = new ConstDefsAST();
  ast->constdefs=unique_ptr<BaseAST>($1);
  ast->constdef=unique_ptr<BaseAST>($3);
  ast->mode=1;
  $$=ast;
}
;

ConstDef
:IDENT '=' ConstInitVal{
  auto ast = new ConstDefAST();
  ast->s=*unique_ptr<string>($1);
  ast->constinitval=unique_ptr<BaseAST>($3);
  ast->mode=0;
  $$=ast;
}
|IDENT{
  auto ast = new ConstDefAST();
  ast->s=*unique_ptr<string>($1);
  ast->mode=1;
  $$=ast;
} 
|IDENT ConstExp1 '=' ConstInitVal{
  auto ast = new ConstDefAST();
  ast->s=*unique_ptr<string>($1);
  ast->constinitval=unique_ptr<BaseAST>($4);
  ast->exp=unique_ptr<BaseAST>($2);
  ast->mode=2;
  $$=ast;
}
;


ValDef
:IDENT '=' InitVal{
  auto ast = new ValDefAST();
  ast->s=*unique_ptr<string>($1);
  ast->constinitval=unique_ptr<BaseAST>($3);
  ast->mode=0;
  $$=ast;
}
|IDENT{
  auto ast = new ValDefAST();
  ast->s=*unique_ptr<string>($1);
  ast->mode=1;
  $$=ast;
} 
| IDENT ConstExp1 '=' InitVal{
  auto ast = new ValDefAST();
  ast->s=*unique_ptr<string>($1);
  ast->constinitval=unique_ptr<BaseAST>($4);
  ast->exp=unique_ptr<BaseAST>($2);
  ast->mode=2;
  $$=ast;
}
|IDENT ConstExp1{
  auto ast = new ValDefAST();
  ast->s=*unique_ptr<string>($1);
  ast->constinitval=unique_ptr<BaseAST>($2);
  ast->mode=3;
  $$=ast;
}
;
InitVal
:Exp{
  auto ast = new ConstInitValAST();
  ast->constexp=unique_ptr<BaseAST>($1);
  ast->mode=0;
  $$=ast;
}
| '{' Exps '}'{
  auto ast = new ConstInitValAST();
  ast->constexp=unique_ptr<BaseAST>($2);
  ast->mode=1;
  $$=ast;
}
| '{''}'{
  auto ast = new ConstInitValAST();
  ast->mode=2;
  $$=ast;
}
;
Exps
:InitVal ',' Exps{
  auto ast = new ExpsAST();
  ast->exp1=unique_ptr<BaseAST>($1);
  ast->exp2=unique_ptr<BaseAST>($3);
  ast->mode=0;
  $$=ast;
}
| InitVal{
  auto ast = new ExpsAST();
  ast->exp1=unique_ptr<BaseAST>($1);
  ast->mode=1;
}
;
ConstInitVal
:ConstExp{
  auto ast = new ConstInitValAST();
  ast->constexp=unique_ptr<BaseAST>($1);
  ast->mode=0;
  $$=ast;
}
| '{' ConstExp2 '}'{
  auto ast = new ConstInitValAST();
  ast->constexp=unique_ptr<BaseAST>($2);
  ast->mode=1;
  $$=ast;
}
| '{' '}'{
  auto ast = new ConstInitValAST();
  ast->mode=2;
  $$=ast;
}
;
ConstExp1
:ConstExp1 '[' ConstExp ']'{
  auto ast = new ConstExp1AST();
  ast->exp1=unique_ptr<BaseAST>($1);
  ast->exp2=unique_ptr<BaseAST>($3);
  ast->mode=0;
  $$=ast;
}
|'[' ConstExp ']'{
  auto ast = new ConstExp1AST();
  ast->exp1=unique_ptr<BaseAST>($2);
  ast->mode=1;
  $$=ast;
}
;
ConstExp2
:ConstInitVal ',' ConstExp2{
  auto ast = new ConstExp2AST();
  ast->exp1=unique_ptr<BaseAST>($1);
  ast->exp2=unique_ptr<BaseAST>($3);
  ast->mode=0;
  $$=ast;
}
| ConstInitVal{
  auto ast = new ConstExp2AST();
  ast->exp1=unique_ptr<BaseAST>($1);
  ast->mode=1;
  $$=ast;
}
;
ConstExp
:Exp{
  auto ast = new ConstExpAST();
  ast->exp=unique_ptr<BaseAST>($1);
  $$=ast;
}
;

%%

// 定义错误处理函数, 其中第二个参数是错误信息
// parser 如果发生错误 (例如输入的程序出现了语法错误), 就会调用这个函数
void yyerror(unique_ptr<BaseAST> &ast, const char *s) {
  cerr << "error: " << s << endl;
}